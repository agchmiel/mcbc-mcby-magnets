import pandas as pd
import matplotlib.pyplot as plt
import PlottingParameters as plparam
from datetime import datetime
import matplotlib.dates as mdates
import dateutil

def AnalyzeFile(file):
    full_path = "C:\\Users\\agchmiel\\cernbox\\Documents\\Papers\\Magnets\\MCBC\\Data\\{}.csv".format(file)
   
    df = pd.read_csv(full_path)
    
    plt.figure()
    
    df.index = pd.to_datetime(df['time'], unit='s')
    for idx, label in enumerate(df.columns[1:], start=1):
        value = df.values[:,idx]
        plt.plot(df.index, value, label=label)

    xfmt = mdates.DateFormatter('%d-%m-%y \n %H:%M')
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    
    plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.ylim(-100,112)   
    plt.title(file)    
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.show()  
    
    
# files = ["cods_h_6016","cods_h_6380","cods_h_7299", "cods_h_7441", "cods_v_6016" , "cods_v_6380", "cods_v_7299",   "cods_v_7441"]
# for file in files:
#     AnalyzeFile(file) 
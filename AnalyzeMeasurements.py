import matplotlib.pyplot as plt
import pandas as pd
import PlottingParameters as plparam
import numpy as np
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
from enum import Enum
import sys
import os
from scipy import interpolate
from cycler import cycler
from FitHysteresis import CalculateEzioFit, FitEzio
import FitHysteresis
import copy
import math
from matplotlib.pyplot import ylabel
# plt.rcParams['axes.prop_cycle'] = cycler(color='bgrcmyk')


colors = ["b","g", "r", "c", "m", "y", "k", "silver", "orange"]
plt.rcParams['axes.prop_cycle'] = plt.cycler(color=colors) 

def SetupPlots():
    """ Setup plots. Needs to be run after the 1st cell in a notebook """
    width = 6.8#6.4#5
    height = 4.8 
    factor = 1
    plt.rcParams["figure.figsize"] = [factor*width, factor*height]
    plt.rcParams['xtick.labelsize']=12
    plt.rcParams['ytick.labelsize']=12
    plt.rcParams['figure.dpi'] = 160

SetupPlots()
MAGNET = "MCBC90"  
COIL_LENGTH = 0.223
TAKE_CENTRAL_COIL = False
DECIMAL_PLACES = 6
LOG_LEVEL = 1

def PrettyPrint(arg):
#     if level >= LOG_LEVEL:
    print("[AnalyzeMeasurement][{}][{}]: ".format(MAGNET, "CENTRAL_COIL" if TAKE_CENTRAL_COIL else "ALL_COILS"), arg)

def SelectReference():
    if MAGNET == "MCBC90":
        return Directory.REFERENCE
    elif MAGNET == "MCBY39":
        return Directory.STC2_MCBY 
    
def SelectCurrentFiltering():
    if MAGNET == "MCBC90":
        current_filtering=30
#         print("Current filtering: {} A".format(current_filtering))
        return current_filtering
    elif MAGNET == "MCBY39":
        current_filtering=25
#         print("Current filtering: {} A".format(current_filtering))
        return current_filtering
    
    
class Directory(Enum):
    REFERENCE = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_stair_step_AP1_0A\\HCMCBCA001-TE000090_20201207_095950_AP2_stair_step_AP1_0A\\aperture2",
    REFERENCE_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\AP2_stair_step_AP1_0A\\HCMCBYA001-TE000039_20210203_114341_AP2_strairstep_AP1_0A\\aperture1",                  
    STC2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_stair_step_AP1_80A\\HCMCBCA001-TE000090_20201207_132145_AP2_stair_step_AP1_80A\\aperture2",
    STC3 = 'H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_stair_step_AP1_m80A\\HCMCBCA001-TE000090_20201208_084633_AP2_stair_step_AP1_m80A\\aperture2',
    SP1 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_special_cyle_1\\HCMCBCA001-TE000090_20201208_143811_AP2_special_cyle_1\\aperture2",
    SP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_special_cyle_2\\HCMCBCA001-TE000090_20201209_114122_AP2_special_cyle_2\\aperture2",
    ROXIE1 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_ROXIE\\HCMCBCA001-TE000090_20201210_085456_AP2_ROXIE\\aperture2",
    ROXIE2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_ROXIE\\HCMCBCA001-TE000090_20201210_111532_AP2_ROXIE_part2\\aperture2",
    STC_1_9 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_stair_step_1.9K\\HCMCBCA001-TE000090_20201211_095243_AP2_stair_step_1.9K\\aperture2",
    STC_ultimate_1_9 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC90\\AP2_ramp_ultimate_1.9K\\HCMCBCA001-TE000090_20201211_141109_AP2_ramp_ultimate_1.9K\\aperture2",    
    STC2_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\AP2_strairstep_AP1_77A\\HCMCBYA001-TE000039_20210204_082626_AP2_strairstep_AP1_77A\\aperture1",
    STC3_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\AP2_strairstep_AP1_m77A\\HCMCBYA001-TE000039_20210204_114107_AP2_strairstep_AP1_m77A\\aperture1",
    SP1_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\AP2_special_cyle_1\\HCMCBYA001-TE000039_20210209_091913_cycle_special_1\\aperture1",
    SP2_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\AP2_special_cyle_2\\HCMCBYA001-TE000039_20210210_093217_cycle_special_2\\aperture1",
    SP3_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\AP2_special_cyle_3\\HCMCBYA001-TE000039_20210212_135754_cycle_special_3\\aperture1",
    ROXIE1_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\ROXIE1\\HCMCBYA001-TE000039_20210210_144139_cycle_special_ROXIE1\\aperture1",
    ROXIE2_MCBYdisabled = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\ROXIE2_AP1_disabled\\HCMCBYA001-TE000039_20210211_102732_cycle_special_ROXIE2\\aperture1",
    ROXIE2_MCBY = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBY39\\ROXIE2_AP2_0A_AP1\\HCMCBYA001-TE000039_20210211_144113_cycle_special_ROXIE2\\aperture1",
    INNER_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Inner\\HCMCBXFB100-E9000011_20210217_200649_Inner_stairstep\\aperture2",    
    OUTTER_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Outer\\HCMCBXFB100-E9000011_20210217_224419_Outter_stairstep\\aperture2",    
    COMBINED_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Combined\\HCMCBXFB100-E9000011_20210217_235058_combined_cycle\\aperture2",
    INVERTED_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Inverted\\HCMCBXFB100-E9000011_20210302_131851_stair_step_combined_cadran_2_and_4\\aperture2",
    SIN1_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Sinusoidal_Cycle_1\\HCMCBXFB100-E9000011_20210218_155708_sinusoidal_cycle\\aperture2",
    SIN2_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Sinusoidal_Cycle_2\\HCMCBXFB100-E9000011_20210222_094451_sinusoidal_cycle_2\\aperture2",
    SIN3_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Sinusoidal_Cycle_3\\HCMCBXFB100-E9000011_20210222_111844_sinusoidal_cycle_3\\aperture2",
    SIN4_MCBXFBP2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Sinusoidal_Cycle_4\\HCMCBXFB100-E9000011_20210222_141318_sinusoidal_cycle_4\\aperture2",
    INNER2 = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBXFBP2\\Inner2\\HCMCBXFB100-E9000011_20210217_200649_Inner_stairstep\\aperture2",
    
    def path(self):
        if sys.platform == "win32":
            return self.value[0]
        return WindowsToUnixPath(self.value[0])
    def name(self):
        res = self.value[0].split("\\")[-3]
        if self == Directory.ROXIE1:
            res = "{}1".format(res)
        elif self == Directory.ROXIE2:
            res = "{}2".format(res)
        return res

def WindowsToUnixPath(path):
    return "/" + "/".join(['eos', 'home-a'] + path.split("\\")[3:])

# "b", "g", "r", "c", "m", "y", "k", "silver"
directories = [
    Directory.REFERENCE,
    Directory.REFERENCE_MCBY,
#     Directory.STC2,
#     Directory.STC3,
#     Directory.SP1,
#      Directory.SP2,
# Directory.ROXIE1,
#      Directory.ROXIE2,
]


directory_to_offset = {
    Directory.REFERENCE: 3425,# 150,
    Directory.REFERENCE_MCBY:3345,# 50,
    Directory.STC2: 4029,
    Directory.STC3: 3800,
    Directory.SP1: 2000,
    Directory.SP2: 2000,
    Directory.ROXIE1: 45,
    Directory.ROXIE2: 100,
    Directory.STC_1_9: 4300,
    Directory.STC_ultimate_1_9: 0,
    Directory.STC2_MCBY: 3422, #,
    Directory.STC3_MCBY: 3260,
    Directory.SP1_MCBY: 0,
    Directory.SP2_MCBY: 0,
    Directory.SP3_MCBY: 0,
    Directory.ROXIE1_MCBY: 0,
    Directory.ROXIE2_MCBY: 0,
    Directory.ROXIE2_MCBYdisabled:0,
    Directory.INNER_MCBXFBP2: 2100,
    Directory.OUTTER_MCBXFBP2: 1850,
    Directory.COMBINED_MCBXFBP2: 1600,
    Directory.INVERTED_MCBXFBP2: 1850,
    Directory.SIN1_MCBXFBP2: 0,
    Directory.SIN2_MCBXFBP2: 0,
    Directory.SIN3_MCBXFBP2: 0,
    Directory.SIN4_MCBXFBP2: 0,
    Directory.INNER2: 0,
}

# By default, every directory is cut at DEFAULT_END_TIME if ramp_rate_filtering is applied
DEFAULT_END_TIME = 10500

# These directories will always be cut
directory_to_forced_ending = {
    Directory.ROXIE2: 5100,
    Directory.REFERENCE_MCBY: 10210,
}

directory_to_forced_skipped_interval = {
    Directory.REFERENCE_MCBY: [6747, 6807],
}

def GetNominalField(directory):
    if TAKE_CENTRAL_COIL == False:  
        if directory == Directory.STC_1_9 and MAGNET == "MCBC90":
            nominal_filed = 2.81
    #         print("Nominal field: {} Tm".format(nominal_filed))
            return nominal_filed
        if directory != Directory.STC_1_9 and MAGNET == "MCBC90":
            nominal_filed = 2.26
    #         print("Nominal field: {} Tm".format(nominal_filed))
            return nominal_filed
        if MAGNET == "MCBY39":
            nominal_filed =  2.41
    #         print("Nominal field: {} Tm".format(nominal_filed))
            return nominal_filed
    else:
        if MAGNET == "MCBC90":
            return 2.44 
# This serv es 2 purposes:
# 1) To divide the data into cycles
# 2) For Special filtering. Please note, we omit the items at indices [2, 5, 6]
special_filtering_intervals_to_take = [0, 1, 3, 4]

directory_to_special_filter = { #scan7  [13578,13613] , [13707,13740], [13770, 13803], [13834,13869], [13897, 13932], [13960,13996], [14026, 14061], [14089, 14122], [14216, 14252] 
    Directory.SP1: [  [3121, 4397], [4500, 5780], [6230, 8330], [8945, 10221], [10705, 11980] , [12300, 13280], [13403, 14400]], 
    Directory.SP2: [[3333, 4555], [4721, 5989], [6453, 8524], [9163, 10438], [10926, 12200] , [12504, 13557], [13617, 14640]],
    Directory.ROXIE1: [[3377, 4197], [4198, 5860], [5861, 6718], [6719, 6928], [6929, 7288], [7289, 7710], [7711, 8070], [8071, 8304]], #[0, 875], [876, 2537], [2538, 4197]
    Directory.ROXIE2: [[100, 935], [936, 2581], [2582,3555],[3556, 3760], [3761, 4125], [4126, 4547], [4548, 4914], [4915, 5110]],
#     Directory.REFERENCE: [[3474, 5273], [5274, 7025], [7026, 8784], [8785, 10522]], 
    Directory.SP1_MCBY: [ [3082, 4359], [4470, 5742], [6204, 7890], [9520, 10785], [11283, 12547], [12875, 13915], [13981, 14991]],
    Directory.SP2_MCBY: [[3350, 4624], [4735, 6010], [6468, 8308], [9300, 10569], [11065, 12331], [12656, 13758], [13760, 14790]],
    Directory.SP3_MCBY: [[1460, 2084], [2305, 2905], [3530, 4458]],
    Directory.ROXIE1_MCBY: [ [3274, 6976], [6532, 6744], [6797, 7108], [7160, 7673], [7728, 8737], [8496, 9404]],
    Directory.ROXIE2_MCBYdisabled:  [ [101, 3386], [3442, 3654], [3709, 4019], [4072, 4584], [4635, 5350], [5401, 6314]],
}

directory_to_cycles = {
    Directory.REFERENCE: [
        {
            "interval": [3309, 5045],
            "delta": 1,
        },
        {
            "interval": [5046, 8512],
            "delta": -1,
        },
        {
            "interval": [8513, 10208],
            "delta": 1,
        },
    ],
    Directory.REFERENCE_MCBY: [
        {
            "interval": [3379, 5042],
            "delta": 1,
        },
        {
            "interval": [5043, 8503],
            "delta": -1,
        },
        {
            "interval": [8504, 10546],
            "delta": 1,
        },
    ],
    Directory.STC2_MCBY: [
        {
            "interval": [3457, 5180],
            "delta": 1,
        },
        {
            "interval": [5181, 8647],
            "delta": -1,
        },
        {
            "interval": [8648, 10517],
            "delta": 1,
        },
    ],
}


def ReadOutputFiles(directory, ramp_rate_filter = True, current_filtering = None, special_filtering = False, include_offset=True, suffix=""):
#     print(directory.value[0])
    name=directory.value[0].split("\\")[-2]
    files_data={}
    aperture_number = directory.path()[-1]
    for i in range(1,6):
        full_path = "{}{}{}_results_Ap_{}_Seg_{}.txt".format(directory.path(), os.path.sep, name,aperture_number, i)
#         print("reading: {} file: {}".format(i, full_path))
#         with open(full_path) as myFile:
#             text = myFile.read()
        offset = directory_to_offset[directory] if include_offset else 0
        df = pd.read_csv(full_path,sep='\t', skiprows=range(1, offset))        
        
        files_data[i]=df
    files_data = AddFilters(directory, files_data, ramp_rate_filter, current_filtering, special_filtering, suffix)
    
    return files_data

# current_filtering: 30, abs(current) < current_filtering
def AddFilters(directory, files_data, ramp_rate_filter, current_filtering, special_filtering, suffix):
    if current_filtering != None and special_filtering:
        print("Cannot perform current filtering and special filtering at the same time")
        sys.exit(0)
    def shouldIncludeRow(idx):
        myTime = files_data[1]['Time(s)'][idx]
        if ramp_rate_filter: 
            if abs(files_data[1][f'Ramprate{suffix}(A/s)'][idx]) >= 0.001:
                return False;
            if current_filtering != None:
                if abs(files_data[1][f'I{suffix}(A)'][idx]) >= current_filtering:
                    return False
                if myTime >= DEFAULT_END_TIME:
                    return False
            if special_filtering and directory in directory_to_special_filter:
                inside = False
                for interval_idx, interval in enumerate(directory_to_special_filter[directory]):
                    if interval_idx in special_filtering_intervals_to_take:
                        if myTime >= interval[0] and myTime <= interval[1]:
                            inside = True
                if not inside:
                    return False
        if directory in directory_to_forced_ending:
            if myTime >= directory_to_forced_ending[directory]:
                return False
        if directory in directory_to_forced_skipped_interval:
            interval = directory_to_forced_skipped_interval[directory]
            if myTime >= interval[0] and myTime <= interval[1]:
                return False
        return True
    filtered_rows = []
    for idx in range(0, len(files_data[1][f'I{suffix}(A)'])):
        if shouldIncludeRow(idx):
            filtered_rows.append(idx)
    for i in range(1, 6):
        files_data[i] = files_data[i].iloc[filtered_rows]
        
    return files_data

def RotateFields(combined_data, rotation_angle):
    rotated_data = copy.deepcopy(combined_data)
    rotated_data['C_main(T)'] = (np.array(rotated_data['B_main(T)']) + rotated_data['A_main(T)']*1j)*np.exp(-rotation_angle*1j)
    rotated_data['B_main(T)'] = np.real(rotated_data['C_main(T)'])
    rotated_data['A_main(T)'] = np.imag(rotated_data['C_main(T)'])
    for i in range(1, 16):
        rotated_data[f'C{i}(T)'] = (rotated_data[f'B{i}(T)'] + rotated_data[f'A{i}(T)']*1j)*np.exp(-rotation_angle*1j*i)
        rotated_data[f'B{i}(T)'] = np.real(rotated_data[f'C{i}(T)'])
        rotated_data[f'A{i}(T)'] = np.imag(rotated_data[f'C{i}(T)'])
    return rotated_data

def CombinedFields(files_data): # Returns integrated fields
    result = dict()
    mean_fields = ['I(A)', 'I1(A)', 'Ramprate(A/s)', 'Ramprate1(A/s)', 'Time(s)']
    for mean_field in mean_fields:
        if mean_field in files_data[1]:
            result[mean_field] = files_data[1][mean_field]
    coil_fields = ['B_main(T)', 'A_main(T)']
    for i in range(1, 16):
        for d in ['B', 'A']:
            coil_fields.append(f'{d}{i}(T)')
        for d in ['b', 'a']:
            coil_fields.append(f'{d}{i}(Units)')

    if TAKE_CENTRAL_COIL:
        coil_lengths = [0, 0, 1, 0, 0]
    else:
        if MAGNET == "MCBXFBP2":
            L = 0.49734 * 0.997 #at cold
            G = 0.00235 * 0.997 #at cold
            FDI_cal = 1.0039
            coil_lengths = np.array([L, L+G, L+2*G, L+G, L]) * FDI_cal;
        else:
            coil_lengths = np.array([COIL_LENGTH] * 5)
    for coil_field in coil_fields:
        if coil_field in files_data[1]:
            result[coil_field] = np.sum([files_data[j][coil_field]*coil_lengths[j - 1] for j in range(1, 6)],0)
    if MAGNET == "MCBXFBP2":
            result = RotateFields(result, -0.005349040597549889)
    return result

def ComputeField(files_data, column_name='B_main(T)'):
    combined_field_data = CombinedFields(files_data)
    time = combined_field_data['Time(s)'].values
    current = combined_field_data['I(A)'].values
    integrated_B_from_five = combined_field_data[column_name]
    return time, current, integrated_B_from_five

def SelectFieldComponent(files_data, column_name):
    if column_name == 'B_main':
        time, current, BField = ComputeField(files_data, column_name='B_main(T)')
    else:
        time, current, BField = ComputeHarmonic(files_data, column_name)
    return time, current, BField

def ComputeHarmonic(files_data, harmonic, current_column = 'I(A)'):
    if harmonic.islower():
        return ComputeNormalizedHarmonic(files_data, harmonic, current_column)
    else:
        return ComputeNonnormalizedHarmonic(files_data, harmonic, current_column)

def ComputeHarmonicFromSingleSegment(combined_data, harmonic, current_column = 'I(A)'):
    if harmonic.islower():
        return ComputeNormalizedHarmonicFromSingleSegment(combined_data, harmonic, current_column)
    else:
        return ComputeNonnormalizedHarmonicFromSingleSegment(combined_data, harmonic, current_column)

def ComputeNormalizedHarmonic(files_data, harmonic, current_column = 'I(A)'): # only normalized eg. a2, b3
    assert(harmonic[0].islower())
    combined_data = CombinedFields(files_data)
    return ComputeNormalizedHarmonicFromSingleSegment(combined_data, harmonic, current_column)

def ComputeNormalizedHarmonicFromSingleSegment(combined_data, harmonic, current_column = 'I(A)'): # harmonic = a3, b2
    assert(harmonic[0].islower())
    existing_key = f'{harmonic}(Units)'
    if existing_key in combined_data:
        values = combined_data[existing_key]
    else:
        # assume non normalized multipoles are present in file eg. A2, B3
        given_multipole_key = "{}(T)".format(harmonic.upper())
        given_multipole = combined_data[given_multipole_key]
        divider_key = "{}_main(T)".format(harmonic[0].upper())
        values = given_multipole / combined_data[divider_key] * 10**4
    return combined_data['Time(s)'], combined_data[current_column], values

def ComputeNonnormalizedHarmonic(files_data, harmonic, current_column = 'I(A)'):
    assert(harmonic[0].isupper())
    combined_data = CombinedFields(files_data)
    return ComputeNonnormalizedHarmonicFromSingleSegment(combined_data, harmonic, current_column='I(A)')

def ComputeNonnormalizedHarmonicFromSingleSegment(combined_data, harmonic, current_column = 'I(A)'):
    assert(harmonic[0].isupper())
    existing_key = f'{harmonic}(T)'
    if existing_key in combined_data:
        values = combined_data[existing_key]
    else:
        # assume non normalized multipoles are present in file eg. a2, b3
        given_multipole_key = "{}(Units)".format(harmonic.lower())
        given_multipole = combined_data[given_multipole_key]
        multiplier_key = "{}_main(T)".format(harmonic[0])
        values = given_multipole * combined_data[multiplier_key] * 10**(-4)
    return combined_data['Time(s)'], combined_data[current_column], values

def ShowNormalizedHarmonics(directories):
    for multipole_type in ['B', 'A']:
        for i in range(2, 5):
            harmonic = f'{multipole_type}{i}'
            for directory in directories:
                
                files_data = ReadOutputFiles(directory, ramp_rate_filter = True, current_filtering = None)
                
                name=directory.name()
                time, current, harmonic_value = ComputeHarmonic(files_data, harmonic)
                plt.plot(current, harmonic_value, label=name, marker  = "o", linestyle="None",  markersize=plparam.MARKER_SIZE)
            plt.title(harmonic)    
            plt.xlabel("Current [A]")
            plt.ylabel(harmonic+" [units]")
            plt.legend(shadow=True, loc='upper right', ncol=1, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
            plt.show()

def ShowHarmonicVsTimeAndCurrent(directories, harmonic, ylim1=None, ylim2=None):
    for directory in directories:
        PrettyPrint(directory.path())
        files_data = ReadOutputFiles(directory, ramp_rate_filter = True, current_filtering = None)
        name=directory.name()
        time, current, component = ComputeHarmonic(files_data, harmonic)
        fig, ax1 = plt.subplots()
        
        ax1.set_ylabel('Current [A]', color=colors[0])  # we already handled the x-label with ax1
        ax1.plot(time, current, color=colors[0], marker  = "o", linestyle="None", label=name, markersize=plparam.MARKER_SIZE)
        ax1.tick_params(axis='y', labelcolor=colors[0])  
        ax2 = ax1.twinx()
        ax2.set_xlabel('Time [s]')
        ax2.set_ylabel(harmonic+" [units]", color=colors[1])
        ax2.plot(time, component, color=colors[1], marker  = "o", linestyle="None", label=name, markersize=plparam.MARKER_SIZE)
        if ylim1:
            plt.ylim(ylim1[0],ylim1[1])
        ax2.tick_params(axis='y', labelcolor=colors[1])
        plt.title(name+" (Meas)")    
        plt.show()
        
       
        if ylim2:
            plt.plot(current, component, marker  = "o", linestyle="None", label=name, markersize=plparam.MARKER_SIZE)
            plt.xlabel("Current [A]")
            plt.ylabel(harmonic+" [units]")
            plt.ylim(ylim2[0],ylim2[1])
            plt.legend(shadow=True, loc='upper right', ncol=1, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
            plt.show()
    
def ShowIntegratedFieldFromCoils(directories):
    if MAGNET == "MCBXFBP2":
        PrettyPrint(f"Cannot show integrated field from coils for {MAGNET}")
        sys.exit()
    for directory in directories:
        PrettyPrint(directory.path())
        files_data = ReadOutputFiles(directory, ramp_rate_filter = False, current_filtering = None)
        name=directory.name()
        
        current = files_data[1]['I(A)'].values
        integrated_B_from_five=np.zeros(len(current))
        for i in range(1, 6):
            time = files_data[i]['Time(s)'].values
            Bfield = files_data[i]['B_main(T)'].values
            position =  np.ones(len(Bfield))*COIL_LENGTH #  files_data[i]['Lcoil(m)'].values
            integrated_B=Bfield *position
            integrated_B_from_five +=   integrated_B 
            plt.plot(time, integrated_B, label="Seg: {}".format(i))
            ax = plt.gca()
            max_nominal_integrated_field=max(integrated_B)
            ax.text(10200, 0.05*-i-0.15, u'$B$$_{}$$_{}$ =  {} Tm'.format("i", i, round(max_nominal_integrated_field,3)), color=colors[i-1], fontsize=plparam.ANNOTATION_FONTSIZE)
        ax.text(9880, -0.05*6-0.15, u'$B_i$$_{}$$_{}$$_{}$ =  {} Tm'.format("1", "-","5",round(max(integrated_B_from_five),3)), color="k", fontsize=plparam.ANNOTATION_FONTSIZE)

        plt.xlabel("Time [s]")
        plt.ylabel("Integrated field [Tm]")
        plt.title(MAGNET+": "+name)
        plt.legend(shadow=True, loc='upper right', ncol=1, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.show()

def FieldFromCentralCoil(files_data):
    current = files_data[3]['I(A)'].values
    time = files_data[3]['Time(s)'].values
    Bfield = files_data[3]['B_main(T)'].values
    return time, current, Bfield

def ShowFieldFromAllCoils(directories, column_name='B_main'):
    for directory in directories:
        PrettyPrint(directory.path())
        files_data = ReadOutputFiles(directory, ramp_rate_filter = False, current_filtering = None)
        name=directory.name()
        current = files_data[1]['I(A)'].values
        for segment_number in range(1, 6):
            if column_name == 'B_main':
                time = files_data[segment_number]['Time(s)'].values
                BField = files_data[segment_number]['B_main(T)'].values
            else:
                time, harmonic_current, BField = ComputeHarmonicFromSingleSegment(files_data[segment_number], column_name)
            plt.plot(time, BField, label="Seg: {}".format(segment_number))
            ax = plt.gca()
            max_nominal_field=max(BField)
            if column_name == 'B_main':
                ax.text(10200, 0.2*-segment_number-0.3, u'$B$$_{}$$_{}$ =  {} T'.format("i", segment_number, round(max_nominal_field,2)), color=colors[segment_number-1], fontsize=plparam.LEGEND_FONTSIZE)
        
        if column_name == 'B_main':
            y_label='B1'
        else:
            y_label=column_name
        plt.xlabel("Time [s]")
        plt.ylabel(f"{y_label} [T]")
        plt.title(MAGNET+": "+name)
        plt.legend(shadow=True, loc='upper right', ncol=1, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.show()      

def ComputeCommonLinearFit(column_name = 'B_main'):
    reference_directory = SelectReference()
    PrettyPrint("Selected reference: {}".format(reference_directory))
    return ComputeLinearFit(reference_directory, column_name)
    
def LinearFit(current, column_name='B_main'):
    c = ComputeCommonLinearFit(column_name)
    linearfit_array =[]
    for value in current:
        linearfit_array.append(c[1]+c[0]*value) 
    
    return linearfit_array  

def ComputeLinearFitInternal(x, y):
    return np.polyfit(x, y, 1, full=False)

def ComputeLinearFit(directory, column_name='B_main'):
    files_data = ReadOutputFiles(directory, ramp_rate_filter = True, current_filtering = SelectCurrentFiltering())
    time, current, BField = SelectFieldComponent(files_data, column_name)
    c = ComputeLinearFitInternal(current, BField)
    PrettyPrint("Fit ({}): y={}*x {} {}".format(column_name, round(c[0],DECIMAL_PLACES), "+" if c[1]>0 else "-", round(abs(c[1]),DECIMAL_PLACES)))
    return c

def ComputeLinearFitWithFit(current, c):
    linearfit_array =[]
    for value in current:
        linearfit_array.append(c[1]+c[0]*value) 
#     print("y={}*x+{}".format(c[0], c[1]))
    return linearfit_array

def LinearFitReproducibility(directory, current):
    c=ComputeLinearFit(directory)
    return ComputeLinearFitWithFit(current, c)

def Residuals(current, BField, column_name = 'B_main'):
    linear_fit=LinearFit(current, column_name)
    residual_array=[]
    for idx in range(len(linear_fit)):
        residual=(BField[idx]-linear_fit[idx])
        residual_array.append(residual)
    return residual_array

def EvaluateFit(current, residuals, a = FitHysteresis.a_original, b = FitHysteresis.b_original, c= FitHysteresis.c_original):
    errors = []
    for idx in range(len(current)):
        fit1 = FitEzio(current[idx], 1, a, b, c)
        fit2 = FitEzio(current[idx], -1, a, b, c)
        my_error = min( abs(residuals[idx] - fit1), abs(residuals[idx] - fit2) )
        errors.append(my_error)
    return errors

def ReadFilesData(directories, ramp_rate_filter=True, current_filtering=None, special_filtering=False, include_offset=True):
    directories_data={}
    for directory in directories:
        files_data= ReadOutputFiles(directory, ramp_rate_filter, current_filtering, special_filtering, include_offset)
        directories_data[directory]=files_data
    return directories_data

def HarmonicForDirectory(directory, harmonic):
    files_data = ReadOutputFiles(directory, ramp_rate_filter = True, current_filtering = None)
    return ComputeHarmonic(files_data, harmonic)

def CycleCurrentForDirectory(directory, ramp_rate_filter = False, current_filtering = None, include_offset=False, suffix=""):
    files_data = ReadOutputFiles(directory, ramp_rate_filter = ramp_rate_filter, current_filtering=current_filtering, include_offset=include_offset, suffix=suffix)
    time = files_data[1]['Time(s)']
    current = files_data[1][f'I{suffix}(A)']
    return time, current

def HasExtraColumnForDirectory(directory, column_name):
    files_data = ReadOutputFiles(directory)
    return column_name in files_data[1]

def ShowCycleCurrent(directories, ramp_rate_filter=False, current_filtering = None, include_offset=False):
    for directory in directories:
        PrettyPrint(directory.path())
        name=directory.name()
        time, current = CycleCurrentForDirectory(directory, ramp_rate_filter, current_filtering, include_offset)

        if HasExtraColumnForDirectory(directory, 'I1(A)'): # current_1 is present
            time_1, current_1 = CycleCurrentForDirectory(directory, ramp_rate_filter, current_filtering, include_offset, suffix="1")
            plt.plot(time, current, label="{}: I(A)".format(name), marker="o", linestyle="None", markersize=1.2, color="blue")
            plt.plot(time_1, current_1, label="{}: I1(A)".format(name), marker="o", linestyle="None", markersize=1.2, color="red")
        else:
            plt.plot(time, current, label="{}".format(name), marker="o", linestyle="None", markersize=1.2)
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Time [s]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.title("Standard cycle")
    if MAGNET == "MCBY39" or MAGNET == "MCBC90":
        plt.ylim(-100,105)
    plt.show()

# Probably remove and use ShowCycleCurrent
def ShowRoxieCycleCurrent():
    directories = [Directory.ROXIE1, Directory.ROXIE2]
    for directory in directories:
        time, current = CycleCurrentForDirectory(directory, ramp_rate_filter = False, current_filtering = None)
        plt.plot(time, current, label="{}".format(directory.name()), marker="o", linestyle="None", markersize=1.2)   
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Time [s]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.title("Special Roxie cycle")
    plt.ylim(-100,105)
    plt.show()
 
def ShowComputedField(directories, column_name='B_main'):
    directories_data = ReadFilesData(directories, ramp_rate_filter=False, special_filtering=False)
    for directory in directories:
        PrettyPrint("Nominal field: {} {}".format(GetNominalField(directory), "T" if TAKE_CENTRAL_COIL else "Tm"))
        files_data = directories_data[directory]
        name=directory.name()
        time_array, current_array, BField_array = SelectFieldComponent(files_data, column_name)
        ax = plt.gca()
        ax.tick_params(axis='y', which='minor', bottom=True)  
        ax.yaxis.grid(True, which='both')
        ax.yaxis.set_minor_locator(AutoMinorLocator())
        plt.plot(time_array, BField_array, label="{}".format(name), marker="o", linestyle="None", markersize=1.2) 
        
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.ylabel(GetBFieldLabel(column_name),  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Time [s]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.show()
    
def ShowTransferFunction(directories, column_name='B_main(T)', yzoom=None, current_column_names=None):
    if current_column_names == None:
        current_column_names = ['I(A)'] * len(directories)
    directories_data = ReadFilesData(directories, ramp_rate_filter=True, special_filtering=False)
    sign_multiple = -1 if column_name == 'A_main(T)' else 1
    for directory, current_column_name in zip(directories, current_column_names):
        files_data = directories_data[directory]
        name=directory.name()
        combined_data = CombinedFields(files_data)
        current = combined_data[current_column_name]
        transfer_function = sign_multiple * combined_data[column_name] / current
        plt.plot(current, transfer_function, label="{}".format(name), marker="o", linestyle="None", markersize=1.2)   
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.ylabel('TF [Tm/A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    if MAGNET == "MCBC90" or MAGNET == "MCBY39":
        plt.ylim(0.02825,0.02885)
    if yzoom:
        plt.ylim(yzoom[0],yzoom[1])
    plt.show()

def GetBFieldLabel(column_name='B_main'):
    if column_name == 'B_main' and TAKE_CENTRAL_COIL == True:
        return 'B1 [T]' 
    if column_name == 'B_main' and TAKE_CENTRAL_COIL == False:
        return 'Integrated B1 [Tm]'
    if TAKE_CENTRAL_COIL == True:
        return f'{column_name} [T]'
    if TAKE_CENTRAL_COIL == False:
        return f'Integrated {column_name} [Tm]'
    
def GetResidualLabel(column_name = 'B_main'):
    if column_name == 'B_main' and TAKE_CENTRAL_COIL == True:
        return 'B1 - B1 (fit) [T]' 
    if column_name == 'B_main' and TAKE_CENTRAL_COIL == False:
        return 'Integrated B1 - Integrated B1 (fit) [Tm]'
    if TAKE_CENTRAL_COIL == True:
        return f'{column_name} - {column_name} (fit) [T]'
    if TAKE_CENTRAL_COIL == False:
        return f'Integrated {column_name} - Integrated {column_name} (fit) [T]'


def ShowLinearFit(directories, column_name='B_main'):
    directories_data = ReadFilesData(directories, ramp_rate_filter=True, current_filtering=SelectCurrentFiltering(), special_filtering=False)
    c = ComputeCommonLinearFit(column_name)
    
    for directory in directories:
        files_data = directories_data[directory]
        name=directory.name()
        time, current, Bfield = SelectFieldComponent(files_data, column_name)
        plt.plot(current, Bfield, label="{}".format(name), marker="o", linestyle="None", markersize=2)             
        linearfit_array = LinearFit(current, column_name)
        plt.plot(current, linearfit_array, label="Linear fit ({})".format(name), marker="None", linestyle="-")   
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.ylabel(GetBFieldLabel(column_name),  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.title("Linear Fit")
        ax=plt.gca()
        if c[1]>0:    
            ax.text(SelectCurrentFiltering()*-1, 0.0, f"y={round(c[0],DECIMAL_PLACES)}x+{abs(round(c[1],DECIMAL_PLACES))}", color = "green", fontsize=plparam.TITLE_FONTSIZE)
        else:
            ax.text(SelectCurrentFiltering()*-1, 0.0, f"y={round(c[0],DECIMAL_PLACES)}x-{abs(round(c[1],DECIMAL_PLACES))}", color = "green", fontsize=plparam.TITLE_FONTSIZE)
            
            
    plt.show()

def AnalyzeLinearFitReproducibility(directories):
    current = np.linspace(-80,80,10000)
    for idx, directory in enumerate(directories):
        name=directory.name()
        c = ComputeLinearFit(directory)
        linearfit_array=LinearFitReproducibility(directory, current)
        plt.plot(current, linearfit_array, label="Linear fit ({})".format(name), marker="None", linestyle="-")   
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.ylabel(GetBFieldLabel(),  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.title("Reproducibility of the linear fit")
        ax=plt.gca()
        if c[1]<0:
            ax.text(20, -0.2*idx, "y={:.6e}x-{:.6e}".format(round(c[0],8), abs(round(c[1],8))), color = colors[idx]) 
        elif c[1]>0:
            ax.text(20, -0.2*idx, "y={:.6e}x+{:.6e}".format(round(c[0],8), round(c[1],8)), color = colors[idx]) 
    plt.show()       
def CalculateBestFit():
    files_data = ReadOutputFiles(SelectReference(), ramp_rate_filter = True, current_filtering = SelectCurrentFiltering())
    time, current, integrated_B_from_five =  ComputeField(files_data)
    residuals=Residuals(current, integrated_B_from_five)
    best_a = FitHysteresis.a_original
    best_b =FitHysteresis.b_original
    best_c = FitHysteresis.c_original
    percentages = None
    best_error = 1000 
    for a_percentage in range(90, 110):
        for b_percentage in range(95, 120):
            for c in np.arange(3, 4, 0.1):
                a = FitHysteresis.a_original * a_percentage / 100
                b = FitHysteresis.b_original * b_percentage / 100
                errors = EvaluateFit(current, residuals, a, b, c)
                total_error = np.sum(errors)
#                 print(a_percentage, a, b_percentage, c, total_error)
                if total_error < best_error:
                    percentages = (a_percentage, b_percentage, c)
                    best_a = a
                    best_b = b
                    best_c = c
                    best_error = total_error
#                 print(a_percentage, b_percentage, c, total_error)
    print("best error: ", best_error)
    print(percentages)
    print("original error: ", np.sum(EvaluateFit(current, residuals)))
    return best_a, best_b, best_c

def SeparateBranches(directory):
    files_data = ReadOutputFiles(directory, ramp_rate_filter = True, current_filtering = None, special_filtering = False)
    cycles = directory_to_cycles[directory]
    ramp_up_rows = []
    ramp_down_rows = []
    for i in range(len(files_data[1]['Time(s)'])):
        timestamp = files_data[1]['Time(s)'].values[i]
        for cycle in cycles:
#             print(timestamp)
            if timestamp >= cycle["interval"][0] and timestamp <= cycle["interval"][1]:
                if cycle["delta"] == -1:
                    ramp_down_rows.append(i)
                else:
                    ramp_up_rows.append(i)
    ramp_down_file_data = {}
    ramp_up_file_data = {}
    for i in range(1, 6):
        ramp_down_file_data[i] = files_data[i].iloc[ramp_down_rows]
        ramp_up_file_data[i] = files_data[i].iloc[ramp_up_rows]
    return ramp_down_file_data, ramp_up_file_data

def EvaluateMagnetization(directory):
    ramp_down_file_data, ramp_up_file_data = SeparateBranches(directory)
    time_down, current_down, integrated_down = ComputeField(ramp_down_file_data)
    time_up, current_up, integrated_up = ComputeField(ramp_up_file_data)
    plt.plot(current_down, integrated_down, label="Ramp down", linestyle="None", marker="x", markersize=2)
    plt.plot(current_up, integrated_up, label="Ramp up", linestyle="None", marker="o", markersize=2)

    
#     integrated_interpolated_up = np.interp(current_down, current_up, integrated_up)
    f = interpolate.interp1d(current_up, integrated_up, fill_value="extrapolate")
    integrated_interpolated_up = f(current_down)
    plt.plot(current_down, integrated_interpolated_up, label="Ramp up interpolated", linestyle="None", marker="d", markersize=2)
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.show()
    
    plt.plot(current_down, integrated_down - integrated_interpolated_up, linestyle="None", marker="o", markersize=2)
    plt.ylabel('Integrated B1 up - Integrated B1 down [Tm]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.show()

def ShowResidualsWithFit(directory, zoom = None):#BUG
    directories = [SelectReference()]
    directories_data = ReadFilesData(directories, ramp_rate_filter=True, current_filtering=None, special_filtering=True)
    for directory in directories:
        files_data = directories_data[directory]
        name=directory.name()
        time, current, integrated_B_from_five =  ComputeField(files_data)
        residuals=Residuals(current, integrated_B_from_five)
        plt.plot(current, residuals, label="{}".format(name), marker="o", linestyle="None", markersize=2)  
    plt.title("Residuals with Fit") 
    plt.ylabel('Integrated B1 - Integrated B1 (fit) [Tm]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})

    xEzio, yEzio = CalculateEzioFit()
    best_a, best_b, best_c = CalculateBestFit()
#     print("Best params: a={}, b={}, c={}".format(best_a, best_b, best_c))
    xEzioBest, yEzioBest = CalculateEzioFit(a = best_a, b = best_b, c = best_c)
    plt.plot(xEzio, yEzio, linestyle="None", marker="x", markersize=2, label="Ezio original fit")
    plt.plot(xEzioBest, yEzioBest, linestyle="None", marker="o", markersize=2, label="Best fit")
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    if zoom:
        plt.xlim(-zoom[0], zoom[0])
        plt.ylim(-zoom[1], zoom[1])
    plt.show()
# ShowResidualsWithFit(Directory.REFERENCE)


# ShowComputeField([Directory.REFERENCE])

def AddAnnotation(currents, residuals, color, position, unitsAbsolute=True):
    ymax=-1000
    ymin=1000
#     print("current len: {} residuals len: {}".format())
#     print(f"current len: {len(current)} residuals len: {len(residuals)}")
#     print(current)
    for current, residual in zip(currents, residuals):
        if current < SelectCurrentFiltering() and current >-SelectCurrentFiltering():            
            if residual>ymax:
                ymax=residual
            if residual<ymin:
                ymin=residual
    find_minimum_current=[]
    for current in currents:
        if current>0 and current<1:
            find_minimum_current.append(current)
    if len(find_minimum_current) == 0: 
        return
    minimum_current=min(find_minimum_current)
    print(minimum_current)
    ax = plt.gca()
    if unitsAbsolute:
        plt.annotate(text='', xy=(minimum_current,-0.000023), xytext=(minimum_current,1.02*ymax), arrowprops=dict(arrowstyle='<->',  color=color))
        if TAKE_CENTRAL_COIL:
            ax.text(6, position, u'$\Delta$$_{}$ = $\pm$ {:.5f} T'.format("a",round(ymax,5)), color=color, fontsize=plparam.ANNOTATION_FONTSIZE)
        else:
            ax.text(6, position, u'$\Delta$$_{}$ = $\pm$ {:.5f} Tm'.format("a", round(ymax,5)), color=color, fontsize=plparam.ANNOTATION_FONTSIZE)
        plt.annotate(text='', xy=(minimum_current,0.000023), xytext=(minimum_current,1.02*ymin), arrowprops=dict(arrowstyle='<->', color=color) )   
   
    else:
        plt.annotate(text='', xy=(minimum_current,-0.01), xytext=(minimum_current,1.02*ymax), arrowprops=dict(arrowstyle='<->',  color=color))
        ax.text(6, position, u'$\Delta$$_{}$ = $\pm$ {} ‰'.format("r",round(ymax,2)), color=color, fontsize=plparam.ANNOTATION_FONTSIZE)
        plt.annotate(text='', xy=(minimum_current,0.01), xytext=(minimum_current,1.02*ymin), arrowprops=dict(arrowstyle='<->', color=color) )
    
def ResidualsForDirectory(directory, column_name='B_main', ramp_rate_filter=True, special_filtering = False):
    files_data = ReadOutputFiles(directory, ramp_rate_filter=ramp_rate_filter, special_filtering=special_filtering)
    time, current, BField = SelectFieldComponent(files_data, column_name)
    residuals = Residuals(current, BField, column_name)
    return time, current, residuals

def ShowResidualsAbsolute(directories, column_name, ramp_rate_filter, special_filtering):
    for directory in directories:
        name=directory.name()
        time, current, residuals = ResidualsForDirectory(directory, column_name, ramp_rate_filter, special_filtering)
        plt.plot(current, residuals, label="{}".format(directory.name()), marker="o", linestyle="None", markersize=2)  
    plt.title("Residuals") 
    #     for i in zip(range(len(current)), current, residuals):   
    #         ax=plt.gca()                                    # <--
    #         ax.annotate(i[0] , xy=(i[1],i[2]), textcoords='data')
    plt.ylabel(GetResidualLabel(column_name),  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})    
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.show()

def ShowResidualsAbsoluteZoom(directories, column_name, ramp_rate_filter, special_filtering, zoom, should_annotate):
#     shouldAddAnnotation = True if len(directories)==1 else False
    for idx, directory in enumerate(directories):
        name=directory.name()
        time, current, residuals = ResidualsForDirectory(directory, column_name, ramp_rate_filter, special_filtering)
        plt.plot(current, residuals, label="{}".format(directory.name()), marker="o", linestyle="None", markersize=2)  
        plt.title("Residuals: Zoom (absolute units)") 
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.ylabel(GetResidualLabel(column_name),  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        ax = plt.gca()
        ax.yaxis.get_ticklocs(minor=True)     # []
        ax.minorticks_on()
        ax.xaxis.set_tick_params(which='minor', bottom=False)
#         plt.ylim(-0.0012, 0.0012)
#         plt.xlim(-30,30)
        plt.xlim(-zoom[0], zoom[0])
        plt.ylim(-zoom[1], zoom[1])
        if should_annotate:
            AddAnnotation(current, residuals, color=colors[idx], position=-idx*0.00011, unitsAbsolute=True)
    
    plt.show()

def ShowResidualsRelativeZoom(directories, column_name, ramp_rate_filter, special_filtering, zoom, should_annotate):
#     shouldAddAnnotation = True if len(directories)==1 else False
    for idx, directory in enumerate(directories):
        time, current, residuals = ResidualsForDirectory(directory, column_name, ramp_rate_filter, special_filtering)
        residualsInpermils = [element / GetNominalField(directory)*1000 for element in residuals]
        PrettyPrint("Normalization: {} {}".format(GetNominalField(directory), "T" if TAKE_CENTRAL_COIL else "Tm"))
        plt.plot(current, residualsInpermils, label="{}".format(directory.name()), marker="o", linestyle="None", markersize=2)  
        plt.title("Residuals: Zoom (relative units)") 
    #     for i in zip(range(len(current)), current, residuals):   
    #         ax=plt.gca()                                    # <--
    #         ax.annotate(i[0] , xy=(i[1],i[2]), textcoords='data')
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        if column_name == 'B_main':
            field_label = 'B1'
        else:
            field_label = column_name
        plt.ylabel(u'Normalized ${\Delta}$'+'{} [‰]'.format(field_label),  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        ax = plt.gca()
        ax.yaxis.get_ticklocs(minor=True)     # []
        ax.minorticks_on()
        ax.xaxis.set_tick_params(which='minor', bottom=False)
#         plt.xlim(-30,30)
#         plt.ylim(-0.5, 0.5)    
        plt.xlim(-zoom[0], zoom[0])
        plt.ylim(-zoom[1], zoom[1])
        if should_annotate:
            AddAnnotation(current, residualsInpermils,  color=colors[idx], position=-idx*0.05, unitsAbsolute=False)
    plt.show()
    

def ShowResiduals(directories, column_name='B_main', ramp_rate_filter=True, special_filtering=False, absolute_zoom=(30, 0.0012), relative_zoom=(30, 0.5), should_annotate=False):
    ShowResidualsAbsolute(directories, column_name, ramp_rate_filter, special_filtering)
    ShowResidualsAbsoluteZoom(directories, column_name, ramp_rate_filter, special_filtering, zoom = absolute_zoom, should_annotate=should_annotate)
    ShowResidualsRelativeZoom(directories, column_name, ramp_rate_filter, special_filtering, zoom = relative_zoom, should_annotate=should_annotate)

# ShowResiduals([Directory.REFERENCE])
#Special cycle
def ShowSeparateScans(directories, special_filtering = False, zoom=None):
    directories_data = ReadFilesData(directories, ramp_rate_filter=False, current_filtering=None, special_filtering=False, include_offset=False)
    for directory in directories:
        files_data = directories_data[directory]
        name=directory.name()
    #    intervals for standard cycle: [[3530,5030], [5301,7021],[7022, 8771], [8771, 10500]] #
        time, current, integrated_B_from_five =  ComputeField(files_data)
        
        plt.plot(time, current, label=" [0,17000]", marker="o", linestyle="None", markersize=1.7)
        ShowValuesWithSubCycles(time, time, current, directory_to_special_filter[directory], special_filtering=special_filtering)
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Time [s]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        if zoom:
            plt.xlim(zoom[0], zoom[1])
            plt.ylim(zoom[2], zoom[3])
        plt.show() 

def DivideIntoSubcycles(time, current, values, subcycles, special_filtering):
    if special_filtering:
        special_subcycles = []
        for idx, subcycle in enumerate(subcycles):
            if idx in special_filtering_intervals_to_take:
                special_subcycles.append(subcycle)
        subcycles = special_subcycles
    plt.gca().set_prop_cycle(plt.cycler(color=colors[1:]) )
#     print(subcycles)
    divided_cycles = []
    for interval in subcycles:
        time_window = []
        value_window = [] 
        current_window = []
        for idx in range(0, len(time)):
            if time[idx]>=interval[0] and time[idx]<=interval[1]:
                time_window.append(time[idx])  
                current_window.append(current[idx])
                value_window.append(values[idx])
        divided_cycles.append((time_window, current_window, value_window))
    return divided_cycles

def ShowLinearFitForSubcycles(directory, selected_indexes = None):
    files_data = ReadOutputFiles(directory, ramp_rate_filter=True, special_filtering=False)
    time, current, BField = ComputeField(files_data)
    subcycles = directory_to_special_filter[directory]
    c0_array = []
    divided_cycles = DivideIntoSubcycles(time, current, BField, subcycles, special_filtering = False)
    for cycle_idx, divided_cycle in enumerate(divided_cycles):
        c = ComputeLinearFitInternal(divided_cycle[1], divided_cycle[2])
        c0_array.append(c[0])
        linear_fit = ComputeLinearFitWithFit(divided_cycle[1], c)
        PrettyPrint(c)
        plt.plot(divided_cycle[1], linear_fit, label="scan: {}".format(cycle_idx+1) )
    plt.ylabel('Integrated B1 [Tm]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    name=directory.name()
    plt.title("Linear coeficient ({})".format(name))
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)    
    plt.show()
   
    plt.bar( range(1,len(subcycles)+1), c0_array, color=colors[1:])
    mean = sum(c0_array)/len(c0_array)
    PrettyPrint("Mean: {:.6f} Tm/A".format(mean))
    PrettyPrint("Minimum value for scan {}: {:.6f} Tm/A, Maximum value for scan {}: {:.6f} Tm/A".format(   c0_array.index(min(c0_array))+1 , min(c0_array), c0_array.index(max(c0_array))+1,   max(c0_array)     ))
    all_scan_difference = max(c0_array)-min(c0_array)
    PrettyPrint("Difference: {:.6f} Tm/A, {:0.2f} ‰".format( round(all_scan_difference,6) , 1000*round(all_scan_difference,6)/round(mean,6)  ))
    if selected_indexes:
        vdM_scans= [c0_array[i] for i in selected_indexes] 
        PrettyPrint("Minimum value for vdM scan {}: {:.6f} Tm/A, Maximum value for vdM scan {}: {:.6f} Tm/A".format(vdM_scans.index(min(vdM_scans))+1 , min(vdM_scans), c0_array.index(max(vdM_scans))+1,   max(vdM_scans)     ))
        vdm_difference = max(vdM_scans)-min(vdM_scans)
        PrettyPrint("Difference for vdM scans: {:.6f} Tm/A, {:0.2f} ‰".format( round(vdm_difference,6) , 1000*round(vdm_difference,6)/round(mean,6)))
    
    plt.ylabel('Linear coefficient (Tm/A)',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Scan number',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.title("Linear coeficient ({})".format(name))
    ax = plt.gca()
    ax.yaxis.get_ticklocs(minor=True)     # []
    ax.minorticks_on()
    ax.xaxis.set_tick_params(which='minor', bottom=False)
    plt.axhline(y=mean, label="mean: {}".format(round(mean,6)), color="k" )
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.ylim(mean-2.5e-05, mean+2.8e-05)
    plt.locator_params(axis="x", integer=True, tight=True)

    plt.show()
    
    normalized_coefficient_array = [(i/mean)*1000 for i in c0_array]
    plt.bar(range(1,len(subcycles)+1), normalized_coefficient_array, color=colors[1:])
    plt.ylabel('Normalized linear coefficient [‰]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Scan number',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.title("Normalized linear coeficient ({})".format(name))
    ax = plt.gca()
    ax.yaxis.get_ticklocs(minor=True)     # []
    ax.minorticks_on()
    ax.xaxis.set_tick_params(which='minor', bottom=False)
    plt.ylim((mean-2.5e-05)/mean*1000,(mean+2.8e-05)/mean*1000)
    plt.locator_params(axis="x", integer=True, tight=True)
    plt.show()
    
    
    
    

def ShowValuesWithSubCycles(time, current, values, subcycles, prefix = "", markerstyle=None, markersize=None, includeSegmentIndex=False, special_filtering = False):
    divided_cycles = DivideIntoSubcycles(time, current, values, subcycles, special_filtering)
    for segment_idx, interval_and_single_cycle in enumerate(zip(subcycles, divided_cycles)):
        interval, single_cycle = interval_and_single_cycle
        if includeSegmentIndex:
            label = "{} Seg {}".format(prefix, segment_idx + 1)
        else:
            label = "{} {}".format(prefix, interval)
        if markerstyle == None:
            plt.plot(single_cycle[1], single_cycle[2], label=label, marker="o", linestyle="None", markersize=2.0)
        elif markerstyle:
            plt.plot(single_cycle[1], single_cycle[2], label=label, marker=markerstyle, linestyle="None", markersize=markersize)


def ShowAnalyzedSpecial(directories, ramp_rate_filter=True, special_filtering=False, asymzoom=((-80,80), (-0.0006, 0.0011)), asymzoom_relative=((-10,30), (-0.3, 0.6))):
    directories_data = ReadFilesData(directories, ramp_rate_filter, current_filtering=None, special_filtering=special_filtering, include_offset=True)
    for directory in directories: 
        files_data = directories_data[directory]
        time, current, integrated_B_from_five =  ComputeField(files_data)
        residuals=Residuals(current, integrated_B_from_five)
        ShowValuesWithSubCycles(time, current, residuals, directory_to_special_filter[directory], special_filtering=special_filtering)

        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.ylabel('Integrated B1 - Integrated B1 (fit) [Tm]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.title("Residuals (absolute units)")
        if asymzoom:
            plt.xlim(asymzoom[0])
            plt.ylim(asymzoom[1])
        plt.show() 
       
        residualsInpermils = [element / GetNominalField(directory) * 1000 for element in residuals]
        ShowValuesWithSubCycles(time, current, residualsInpermils, directory_to_special_filter[directory], special_filtering=special_filtering)

        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        plt.ylabel(u'Normalized ${\Delta}$B1 [‰]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})    
        plt.title("Residuals (relative units)") 
        if asymzoom_relative:
            plt.xlim(asymzoom_relative[0])
            plt.ylim(asymzoom_relative[1])
        plt.show() 
    
def SaveResidualsToFile(directories):
    DIRECTORY_TO_WRITE = "H:\\user\\a\\agchmiel\\Documents\\Measurements\\MCBC_Analysis\\ForMichi\\"
    directories_data = ReadFilesData(directories, ramp_rate_filter = True, current_filtering = None, special_filtering = True, include_offset=True)
    for directory in directories:
        files_data = directories_data[directory]
        time, current, integrated_B_from_five = ComputeField(files_data)
        residuals = Residuals(current, integrated_B_from_five)
        extracted_df = pd.DataFrame(zip(current, residuals), columns = ['Current(A)', 'Residual(Tm)'])
        name=directory.name()
        file_path = "{}{}\\{}.csv".format(DIRECTORY_TO_WRITE, MAGNET, name)
        print(file_path)
        extracted_df.to_csv(file_path, index=False)  
# SaveResidualsToFile([Directory.SP1_MCBY, Directory.SP2_MCBY, Directory.SP3_MCBY])
# ShowCycleCurrent([Directory.STC2_MCBY])
ShowComputedField([Directory.REFERENCE])
# 
# ShowIntegratedFieldFromCoils([Directory.REFERENCE])
# ShowIntegratedFieldFromCoils([Directory.ROXIE1_MCBY])
# ShowResiduals([Directory.ROXIE1_MCBY],   ramp_rate_filter = False)
# ShowTransferFunction([Directory.REFERENCE])
# ShowRoxieCycleCurrent()
# ShowFieldFromAllCoils([Directory.REFERENCE])
# AnalyzeLinearFitReproducibility([Directory.REFERENCE, Directory.STC2, Directory.STC3, Directory.SP1, Directory.SP2])
# ShowCycleCurrent([Directory.SP1], ramp_rate_filter=True)
# ShowIntegratedField([Directory.REFERENCE])
# ShowCycleCurrent([Directory.REFERENCE_MCBY])
# ShowCycleCurrent([Directory.STC3_MCBY])
# ShowResiduals([Directory.REFERENCE_MCBY])
# ShowSeparateScans([Directory.REFERENCE_MCBY])
# ShowAnalyzedSpecial([Directory.REFERENCE_MCBY])
# ShowResiduals([Directory.REFERENCE_MCBY, Directory.STC2_MCBY, Directory.STC3_MCBY])

# ShowAnalyzedSpecial([Directory.SP1],  special_filtering=True, asymzoom=((-10,30), (-0.0008, 0.001)),  asymzoom_relative=((-10,30), (-0.3, 0.5)))
# ShowCycleCurrent([Directory.REFERENCE])

# ShowIntegratedField([Directory.REFERENCE])
# ShowResiduals([Directory.REFERENCE_MCBY, Directory.STC2_MCBY, Directory.STC3_MCBY], should_annotate=True)
# ShowResiduals([Directory.REFERENCE], should_annotate=True)
# ShowComputedField([Directory.REFERENCE])

# ShowSeparateScans([Directory.SP1])
# ShowAnalyzedSpecial([Directory.SP1], asymzoom=((-10,30), (-0.0006, 0.0012)))
# ShowSeparateScans([Directory.SP2_MCBY])
# ShowSeparateScans([Directory.SP2_MCBY])
# ShowAnalyzedSpecial([Directory.SP2_MCBY], asymzoom=((-10, 10), (-0.003, 0.003)), asymzoom_relative=((-10,10), (-1, 1)) )
# ShowResiduals([Directory.REFERENCE_MCBY], special_filtering = True, absolute_zoom=(30, 0.0013))
# 
# ShowSeparateScans([Directory.ROXIE2_MCBYdisabled])
# ShowAnalyzedSpecial([Directory.ROXIE2_MCBYdisabled], ramp_rate_filter =False,  special_filtering=False,  asymzoom=((-40, 40), (-0.002, 0.002)), asymzoom_relative=((-40,40), (-1, 1)) )
# ShowHarmonicVsTimeAndCurrent([Directory.REFERENCE], "a3", ylim1=(-50,20), ylim2=(-6,0))
# ShowHarmonics([Directory.REFERENCE], "b3", ylim1=(-50,20), ylim2=(-50,0))
# AnalyzeLinearFitReproducibility([Directory.REFERENCE,Directory.SP2])
# ShowSeparateScans([Directory.SP1])
# ShowLinearFitForSubcycles(Directory.SP2)
# ShowLinearFit([Directory.REFERENCE])

# ShowLinearFitForSubcycles(Directory.SP2, selected_indexes=[0,1,3,4])
# ShowLinearFitForSubcycles(Directory.SP3_MCBY)
# ShowResiduals([Directory.REFERENCE_MCBY, Directory.STC2_MCBY, Directory.STC3_MCBY], should_annotate=True)
# ShowResiduals([Directory.REFERENCE_MCBY, Directory.STC2_MCBY, Directory.STC3_MCBY], should_annotate=True)
# ShowAnalyzedSpecial([Directory.SP3_MCBY], asymzoom=((-10,40), (-0.005, 0.004)), asymzoom_relative=((-10,40), (-2, 1.5)) )
# ShowCycleCurrent([Directory.INNER_MCBXFBP2])
# ShowComputedField([Directory.INNER_MCBXFBP2])
# ShowTransferFunction([Directory.OUTTER_MCBXFBP2, Directory.INVERTED_MCBXFBP2], column_name='A_main(T)') 
# ShowTransferFunction([Directory.COMBINED_MCBXFBP2], column_name='A_main(T)') 
# ShowComputedField([Directory.INNER2])
# ShowTransferFunction([Directory.INNER_MCBXFBP2, Directory.INNER2], column_name='A_main(T)') 
# ShowComputedField([Directory.INVERTED_MCBXFBP2])
# ShowComputedField([Directory.INVERTED_MCBXFBP2])
# ShowTransferFunction([Directory.INNER_MCBXFBP2, Directory.COMBINED_MCBXFBP2, Directory.INVERTED_MCBXFBP2]) 
# ShowTransferFunction([Directory.OUTTER_MCBXFBP2, Directory.COMBINED_MCBXFBP2, Directory.INVERTED_MCBXFBP2], column_name='A_main(T)', current_column_names = ['I(A)', 'I(A)', 'I1(A)'])
# ShowNormalizedHarmonics([Directory.REFERENCE])
# ShowNormalizedHarmonics([Directory.INNER_MCBXFBP2]) #, Directory.OUTTER_MCBXFBP2, Directory.INVERTED_MCBXFBP2])
# ShowCycleCurrent([Directory.INNER_MCBXFBP2], ramp_rate_filter=True, current_filtering=None)
# ShowComputedField([Directory.INNER_MCBXFBP2])
# ShowCycleCurrent([Directory.INNER_MCBXFBP2])
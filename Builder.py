import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle
import PlottingParameters as plparam
import pandas as pd

STEP_SIZE = 5

def step(number_of_steps, step_delta, step_size):
    def compute(start_x, start_value):
        # use flat() to produce flat line
        assert step_delta != 0
        # step_delta must be in the direction from start_value to end_value
        current_value = start_value
        current_x = start_x
        x = []
        y = []
        for step_count in range(number_of_steps):
            for j in range(step_size):
                x.append(current_x)
                y.append(current_value)
                current_x += 1
            current_value += step_delta
        return x, y
    return compute

def flat(duration):
    def compute(start_x, value):
        end_x = start_x + duration
        x=np.arange(start_x, end_x, STEP_SIZE)
        y=np.ones_like(x, dtype=float)*value
        return x, y
    return compute

def jump(delta, duration):
    def compute(start_x, start_value):
        end_value = start_value + delta
        end_x = start_x + duration
        x = np.arange(start_x, end_x + 1, STEP_SIZE)
        y = np.zeros_like(x, dtype=float)
        a = float(end_value - start_value) / (end_x - start_x)
        for idx, x_element in enumerate(x):
            y[idx] = (start_value + (x_element - start_x) * a)
        return x, y
    return compute

def buildSubsycle(maximum_current):
    subcycle = [
        flat(60),
        jump(maximum_current, int(maximum_current*10)),
        flat(60),
        jump(-(2 * maximum_current), int(2 * maximum_current * 10)),
        flat(60),
        jump(maximum_current, int(maximum_current * 10))
    ]
    return subcycle
def constructSpecialRoxie1(title):
    precycle_current=77
    shapes = [
        flat(60),
        jump(precycle_current, precycle_current * 10),
        flat(60),
        jump(-(2 * precycle_current), 2 * precycle_current * 10),
        flat(60),
        jump(2 * precycle_current, 2 * precycle_current * 10),
        flat(60),
        jump(-(2 * precycle_current), 2 * precycle_current * 10),
        flat(60),
        jump(precycle_current, precycle_current * 10), 
        # first small cycle
    ] + buildSubsycle(2.5) + buildSubsycle(5) +  buildSubsycle(10) +  buildSubsycle(15) +  buildSubsycle(20)
#     print(shapes)
    x, y = build(shapes, title)
    return x, y

def constructSpecialRoxie2(title):
    precycle_current=77
    offset_current = 10
    offset = [jump(offset_current, offset_current * 10)]
    end = [jump(-offset_current, offset_current * 10),]
    shapes = [] + buildSubsycle(precycle_current) + offset +  buildSubsycle(2.5) + buildSubsycle(5) +  buildSubsycle(10) +  buildSubsycle(15) +  buildSubsycle(20) +end
#     print(shapes)
    x, y = build(shapes, title)
    return x, y
    

def build(shapes, title):
    x = []
    y = []
    current_x = 0
    current_y = 0
    for shape in shapes:
        x_shape, y_shape = shape(current_x, current_y)
        x = x + list(x_shape)
        y = y + list(y_shape)
        current_x = x_shape[-1]
        current_y = y_shape[-1]
#         print(current_x, current_y)
    plt.plot(x, y, color='#bcbd22', marker="None")
    ax = plt.gca()
    plt.yticks(np.arange(-80, 90, step=10))
    ax.tick_params(axis='x', which='minor', bottom=False)
    plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Time [s]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.title("MCBY "+title)
    plt.grid(b=True, which='both', axis = "y", color='0.65', linestyle='-')
   
    plt.show()
    return x, y
    
def construct(high_current_field, amplitude, cycle_count, step_size, x_max_time, title ):

    # precycle
    # jump180
    high_current_duration=10*60
    number_of_steps = 25
    precycle_current=70
    repeated_shape = [
        flat(high_current_duration),
        jump(high_current_field + amplitude, 60),
        step(number_of_steps, - 2 * amplitude / number_of_steps, step_size),
        jump(high_current_field, 60),
    ]
    
    shapes = [
        flat(200),
        jump(precycle_current, 180),
        flat(20),
        jump(-(precycle_current), 360),
        flat(20),
        jump(0, 180),
        flat(200),
        jump(high_current_field, 60),
    ] + np.tile(repeated_shape, cycle_count).tolist()
    
    x,y = build(shapes, x_max_time)
 
 

def SaveData(x, y, file):
    directory = "C:\\Users\\agchmiel\\cernbox\\Documents\\Measurements\\MCBY39\\Analysis\\"
    values = zip(x,y)
    extracted_df = pd.DataFrame(values, columns = ['Time', 'Current'])
    extracted_df.to_csv("{}{}.csv".format(directory, file), index=False)  
    
# construct(15.5, 23.5, 7, 45, 4 * 60 * 60, "cods_h_6016")
# construct(4.4, 5.25, 3, 35, 4 * 60 * 60, "cods_h_6380")
# construct(0.45, 4.65, 3, 35, 4*60 * 60, "cods_h_7299")

# MCBY special ROXIE cycles
# x,y = constructSpecialRoxie1("ROXIE1")
# SaveData(x, y, "ROXIE1")
# 
# x,y = constructSpecialRoxie2("ROXIE2")
# SaveData(x, y, "ROXIE2")

from AnalyzeMeasurements import Directory, MAGNET
import AnalyzeMeasurements
from matplotlib.pyplot import plot
import AnalyzeOutput
import PlottingParameters as plparam
import numpy as np
import matplotlib.pyplot as plt
from AnalyzeOutput import AllGraphs
# 

colors = ["b","g", "r", "c", "m", "y", "k", "silver", "orange"]

def PrettyPrint(arg):
#     if level >= LOG_LEVEL:
    print("[CompareMeasurementsWithOutput]: ", arg)





roxie_output_to_special_filter = {
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1" : [  [40,51], [51,81], [81,91],[91,111], [111,131], [131,151], [151,161]], # xx[0, 10.5], [11,30.5] , [31, 50.5],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1v1" : [  [121,151], [151,211], [211,241] , [241,271],[271,331], [331,391], [391,451], [451,481]],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1v1_NiiON" : [  [121,151], [151,211], [211,241] , [241,271],[271,331], [331,391], [391,451], [451,481]],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1v1_6mm": [  [121,151], [151,211], [211,241] , [241,271],[271,331], [331,391], [391,451], [451,481]],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1v1_8mm": [  [121,151], [151,211], [211,241] , [241,271],[271,331], [331,391], [391,451], [451,481]],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1v1_14mm": [  [121,151], [151,211], [211,241] , [241,271],[271,331], [331,391], [391,451], [451,481]],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE2" : [  [1,11], [11,31], [31,51],[51,61], [61,71], [71,81], [81,91], [91,101]],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE2v1" : [  [1,31], [31,91], [91,151],[151,211] ,[211,271], [271,331], [331,371]],
    "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE2v3" : [  [1,61], [61,181], [181,241],[241,271] ,[271,307], [307,342], [342,380], [380,397]],

}

def PlotCycles(data, labels, subcycles, figuresize):
    markerstyles = ["o", "x", "d"]
    markersizes = [0.7, 4, 3]
    if figuresize:
        plt.figure(figsize=[figuresize[0], figuresize[1]])
#     print(subcycles, labels)
    for idx, datum in enumerate(data):
        first_subcycle_start = subcycles[idx][0][0]
        
        # Add full cycle plot in blue, if pre-cycle is present
        # Checking the first timestamps vs the first subcycle start
        if datum[0][0] < first_subcycle_start: 
            plt.plot(datum[0], datum[1], label="{} Pre-cycle".format(labels[idx]), marker=markerstyles[idx], markersize= markersizes[idx], linestyle = "None"  )
        AnalyzeMeasurements.ShowValuesWithSubCycles(datum[0], datum[0], datum[1], subcycles[idx], labels[idx], markerstyles[idx] , markersizes[idx], includeSegmentIndex=True)
        plt.ylabel("Current [A]", fontsize=plparam.LABEL_FONTSIZE)
        plt.xlabel("Time [s]", fontsize=plparam.LABEL_FONTSIZE)
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, bbox_to_anchor=(0.5, 1.15), fontsize=plparam.LEGEND_FONTSIZE)
        plt.yticks(np.arange(-100, 130, 20))
        plt.ylim(-90,120)
        if idx == 0:
            plt.show()
            if figuresize:
                plt.figure(figsize=[figuresize[0], figuresize[1]])

    plt.ylabel("Current [A]", fontsize=plparam.LABEL_FONTSIZE)
    plt.xlabel("Time [s]", fontsize=plparam.LABEL_FONTSIZE)
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, bbox_to_anchor=(0.5, 1.15), fontsize=plparam.LEGEND_FONTSIZE)
    plt.yticks(np.arange(-100, 120, 20))
    plt.ylim(-90,120)
    plt.show()
    

def PlotResiduals(data, labels, zoom, assymzoom, annotations, subcycles, figuresize, column_name):
    markerstyles = ["o", "x", "d"]
    markersizes = [0.7, 4, 3]
    if figuresize:
        plt.figure(figsize=[figuresize[0], figuresize[1]])
   
    for idx, datum in enumerate(data):
        if subcycles:
            AnalyzeMeasurements.ShowValuesWithSubCycles(datum[0], datum[1], datum[2], subcycles[idx], labels[idx], markerstyles[idx], markersize= markersizes[idx], includeSegmentIndex=True)
            
        else:
            plt.plot(datum[1], datum[2], marker="o", markersize=2, linestyle="None", label=labels[idx])
        if annotations:
            AddAnnotation(datum[1],  datum[2], "unitsAbsolute", color=colors[idx], position=idx * -0.00015)
    plt.title("Residuals (absolute units)", fontsize=plparam.TITLE_FONTSIZE)    
    plt.ylabel(AnalyzeMeasurements.GetResidualLabel(GetColumnNameForMeasurements(column_name)), fontsize=plparam.LABEL_FONTSIZE)
    plt.xlabel("Current [A]", fontsize=plparam.LABEL_FONTSIZE)
    if subcycles!=None:
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5,  bbox_to_anchor=(0.5, 1.25), fontsize=plparam.LEGEND_FONTSIZE)
    if subcycles==None:
        plt.legend(shadow=True, loc='upper center', ncol=1, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)  
  
    if zoom:
        plt.xlim(-zoom[0], zoom[0])
        plt.ylim(-zoom[1], zoom[1])  
        plt.title("Residuals: Zoom (absolute units)", fontsize=plparam.TITLE_FONTSIZE) 
    if assymzoom:
        plt.xlim(assymzoom[0])
        plt.ylim(assymzoom[1])
    plt.show()
    
    

def AddAnnotation(current, residuals, units, color, position):
    ymax=-1000
    ymin=1000
    for idx in range(0, len(residuals)):
        if current[idx]<30 and current[idx]>-30:            
            if residuals[idx]>ymax:
                ymax=residuals[idx]
            if residuals[idx]<ymin:
                ymin=residuals[idx]
    ax = plt.gca()
    if units == "unitsAbsolute":
        plt.annotate(text='', xy=(0,-0.000023), xytext=(0,1.02*ymax), arrowprops=dict(arrowstyle='<->',  color=color))
        if AnalyzeMeasurements.TAKE_CENTRAL_COIL:
            ax.text(3, position, u'$\Delta$$_{}$ = $\pm$ {} T'.format("a",round(ymax,5)), color=color, fontsize=plparam.ANNOTATION_FONTSIZE)
        else:
            ax.text(3, position, u'$\Delta$$_{}$ = $\pm$ {} Tm'.format("a",round(ymax,5)), color=color, fontsize=plparam.ANNOTATION_FONTSIZE)
        plt.annotate(text='', xy=(0,0.000023), xytext=(0,1.02*ymin), arrowprops=dict(arrowstyle='<->', color=color) )   
    else:
        plt.annotate(text='', xy=(0,-0.01), xytext=(0,1.02*ymax), arrowprops=dict(arrowstyle='<->',  color=color))
        ax.text(3, position, u'$\Delta$$_{}$ = $\pm$ {} ‰'.format("r",round(ymax,2)), color=color, fontsize=plparam.ANNOTATION_FONTSIZE)
        plt.annotate(text='', xy=(0,0.01), xytext=(0,1.02*ymin), arrowprops=dict(arrowstyle='<->', color=color) )

def GetColumnNameForMeasurements(column_name):
    if column_name[1] == '1':
        return f"{column_name[0]}_main"
    else:
        return column_name

def ResidualData(directory, output_files, file_to_graph_data, column_name='B1'):
    ramp_rate_filter = True
    if directory in [Directory.ROXIE1, Directory.ROXIE2]:
        ramp_rate_filter = False
    directory_data = AnalyzeMeasurements.ResidualsForDirectory(directory, column_name=GetColumnNameForMeasurements(column_name), ramp_rate_filter=ramp_rate_filter)
    output_datas = []
    for output_file in output_files:
        output_datas.append(AnalyzeOutput.ResidualsForFile(file_to_graph_data[output_file], column_name))
    return [directory_data] + output_datas

def GetLabels(directory, output_files):
    directory_label = directory.name() + " (Meas)"
    output_file_labels = [output_file.split("\\")[-1] + " (Sim)" for output_file in output_files]
    return [directory_label] + output_file_labels

def AnalyzeCycles(directory, output_files, file_to_graph_data, figuresize=None):
    labels = GetLabels(directory, output_files)
    data = ResidualData(directory, output_files, file_to_graph_data)
    subcycles=[]
    subcycles.append(AnalyzeMeasurements.directory_to_special_filter[directory])
    for output_file in output_files:
        subcycles.append(roxie_output_to_special_filter[output_file])
    PlotCycles(data, labels, subcycles, figuresize)

def AnalyzeResiduals(directory, output_files, file_to_graph_data, column_name, zoom=None, asymzoom=None, annotations=False, show_subcycles = False, figuresize=None, overwrite_labels=None):
    if overwrite_labels:
        labels = overwrite_labels
    else:
        labels = GetLabels(directory, output_files)
    data = ResidualData(directory, output_files, file_to_graph_data, column_name)
    subcycles = None
    if show_subcycles:
        subcycles=[]
        subcycles.append(AnalyzeMeasurements.directory_to_special_filter[directory])
        for output_file in output_files:
            subcycles.append(roxie_output_to_special_filter[output_file])
    PlotResiduals(data, labels, zoom, asymzoom, annotations, subcycles, figuresize, column_name)

def PlotResidualsRelative(data, labels, zoom, annotations, column_name):
    for idx, datum in enumerate(data):
        plt.plot(datum[1], datum[2], marker="o", markersize=2, linestyle="None", label=labels[idx])
        if annotations:
            AddAnnotation(datum[1],  datum[2], "unitsRelative", color=colors[idx], position=idx * -0.05)
    plt.title("Residuals (relative units)", fontsize=plparam.TITLE_FONTSIZE)    
    plt.ylabel(u'Normalized $\Delta$'+f'{column_name} [‰]', fontsize=plparam.LABEL_FONTSIZE)
    plt.xlabel("Current [A]", fontsize=plparam.LABEL_FONTSIZE)
    plt.legend(shadow=True, loc='upper center', ncol=1, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)    
    if zoom:
        plt.xlim(-zoom[0], zoom[0])
        plt.ylim(-zoom[1], zoom[1])  
        plt.title("Residuals: Zoom (relative units)", fontsize=plparam.TITLE_FONTSIZE)
    plt.show()


def ResidualRelativeData(directory, output_files, file_to_graph_data, column_name):
    data = ResidualData(directory, output_files, file_to_graph_data, column_name)
    output_data = []
    for idx, datum in enumerate(data):
        if idx == 0:
            nominal_computed_field_for_directory = AnalyzeMeasurements.GetNominalField(directory)
#             print("nominal_integrated_field for directory", nominal_integrated_field_for_directory)
            relative_residuals = [residual / nominal_computed_field_for_directory * 1000 for residual in datum[2]]
            PrettyPrint("Normalization for {}: {} T".format(directory.path(), nominal_computed_field_for_directory))
        else:
            output_file = output_files[idx - 1]
            nominal_field_for_output = AnalyzeOutput.GetNominalField(file_to_graph_data[output_file][15])
#             print("nominal_field for output ", nominal_field_for_output)
            relative_residuals = [residual / (nominal_field_for_output) * 1000 for residual in datum[2]]
            PrettyPrint("Normalization for {}: {} T".format(output_file, round(nominal_field_for_output,2)))
        output_data.append((datum[0], datum[1], relative_residuals))
    return output_data
    
def AnalyzeResidualsRelative(directory, output_files, file_to_graph_data, column_name, zoom=None, annotations=False):
    data = ResidualRelativeData(directory, output_files, file_to_graph_data, column_name)
    labels = GetLabels(directory, output_files)
    PlotResidualsRelative(data, labels, zoom, annotations, column_name)

def PlotHarmonic(data, labels, harmonic, zoom, meas_bias):
    for idx, datum in enumerate(data):
        plt.plot(datum[1], datum[2], marker="o", markersize=2, linestyle="None", label=labels[idx])
    if meas_bias:
        plt.title("Harmonic {} (meas bias = {})".format(harmonic, meas_bias), fontsize=plparam.TITLE_FONTSIZE)    
    else:
        plt.title("Harmonic {}".format(harmonic), fontsize=plparam.TITLE_FONTSIZE)    
    plt.ylabel("{} [units]".format(harmonic), fontsize=plparam.LABEL_FONTSIZE)
    plt.xlabel("Current [A]", fontsize=plparam.LABEL_FONTSIZE)
    plt.legend(shadow=True, loc='upper center', ncol=1, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)    
    if zoom:
        plt.ylim(zoom)
    plt.show()

def AnalyzeHarmonic(directory, output_files, file_to_graph_data, harmonic, zoom, meas_bias):
    directory_data = AnalyzeMeasurements.HarmonicForDirectory(directory, harmonic)
    output_data = []
    for file in file_to_graph_data:
        output_data.append(AnalyzeOutput.HarmonicForFile(file_to_graph_data[file], harmonic))
    directory_data_biasad = [i + meas_bias for i in directory_data[2]]
    directory_data = (directory_data[0], directory_data[1], directory_data_biasad)
    data = [directory_data] + output_data
    labels = GetLabels(directory, output_files)
    PlotHarmonic(data, labels, harmonic, zoom, meas_bias)

def AnalyzeHarmonics(directory, output_files, file_to_graph_data, harmonics, zooms, meas_biases):
    for harmonic, zoom, meas_bias in zip(harmonics, zooms, meas_biases):
        AnalyzeHarmonic(directory, output_files, file_to_graph_data, harmonic, zoom, meas_bias)

def CompareHarmonics(directory, output_files, harmonics = ["b1", "a1", "b2", "a2", "b3", "a3", "b4", "a4", "b5", "a5"], zooms=None, meas_biases=None):
    if not zooms:
        zooms = [None] * len(harmonics)
    if not meas_biases:
        meas_biases = [0] * len(harmonics)
    file_to_graph_data = AnalyzeOutput.AllGraphs(output_files)   
    AnalyzeHarmonics(directory, output_files, file_to_graph_data, harmonics, zooms, meas_biases)

def Compare(directory, output_files, column_name='B1', absolute_zoom=None, absolute_asymzoom=None, relative_zoom=None, absolute_zoom_annotations=False, relative_zoom_annotations=False, overwrite_labels=None):
    file_to_graph_data = AnalyzeOutput.AllGraphs(output_files)   
    AnalyzeResiduals(directory, output_files, file_to_graph_data, column_name, annotations=False, show_subcycles = False, overwrite_labels = overwrite_labels) 
    AnalyzeResiduals(directory, output_files, file_to_graph_data, column_name, zoom=absolute_zoom, asymzoom = absolute_asymzoom, annotations=absolute_zoom_annotations, overwrite_labels = overwrite_labels)
    AnalyzeResidualsRelative(directory, output_files, file_to_graph_data, column_name, annotations=False)
    AnalyzeResidualsRelative(directory, output_files, file_to_graph_data, column_name, zoom=relative_zoom, annotations=relative_zoom_annotations)
#    
def CompareWithSubcycles(directory, output_files, absolute_assym_zoom=None, figuresize=None):
    file_to_graph_data = AnalyzeOutput.AllGraphs(output_files)  
    AnalyzeCycles(directory, output_files, file_to_graph_data, figuresize=figuresize)
    AnalyzeResiduals(directory, output_files, file_to_graph_data, annotations=False, show_subcycles = True,  figuresize=figuresize)
    AnalyzeResiduals(directory, output_files, file_to_graph_data, asymzoom=absolute_assym_zoom, annotations=False, show_subcycles = True,  figuresize=figuresize)


# directories_to_output_files = {
#     Directory.REFERENCE: [
# #         "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_REF",
#         "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_REFexd",
# #         "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_REF_1_9K",
#     ], 
# #     Directory.STC2: ["C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_STC2"],
# #     Directory.STC3: ["C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_STC3"],
# #       Directory.ROXIE1: [
# #         "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1v1",
# #         "C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE1v1_NiiON",
# #         ],
# #       Directory.ROXIE2: ["C:\\Users\\agchmiel\\cernbox\\Documents\\ROXIE\\OutputFiles\\MCBC-pers_debug80A_ROXIE2v3"],
#    
# }
# for directory in directories_to_output_files:
#     CompareHarmonics(directory, directories_to_output_files[directory], harmonics=["b3", "b5"], zooms=[(-30, 30), (-2, 2)], meas_biases =[26, 1.78])
#     Compare(directory, directories_to_output_files[directory])
#       
# AnalyzeMeasurements.ShowAnalyzedSpecial([Directory.ROXIE1], ramp_rate_filter=False, asymzoom=None)

# Compare(Directory.REFERENCE, [AnalyzeOutput.REFERENCE_FILE], absolute_zoom=(30, 0.0015), relative_zoom=(30, 0.8), absolute_zoom_annotations=True, relative_zoom_annotations=True)

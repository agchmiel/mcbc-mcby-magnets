import pandas as pd
import matplotlib.pyplot as plt
import PlottingParameters as plparam
from datetime import datetime
import matplotlib.dates as mdates
import dateutil
import numpy as np
import sys
from enum import Enum

def GetNthColumn(values, n):
    return [value[n] for value in values]

directory = "C:\\Users\\agchmiel\\cernbox\\Documents\\Papers\\Magnets\\MCBC\\Data\\"
MCBY_precycle = "Precycle_MCBY"


class FilteringType(Enum):
    HORIZONTAL = 'horizontal'
    VERTICAL = 'vertical'

def ReadPrecycle(file):
    df, original_column_ids = ReadCSVFile(file)
    time = df["time"]
    current = df["Current"]
    return time, current
    
def ReadFile(file, columns=None, dateFormat=False, filtering_type=None, figuresize=None, addPrecycle=False):
    df, original_column_ids = ReadCSVFile(file, columns)
    print(file)
    timestamps = df['time']
    if filtering_type != None:
        extracted_values = ExtractedData(df, file, filtering_type)
        extracted_timestamps = GetNthColumn(extracted_values, 0)
    if figuresize!=None:
        plt.figure(figsize=[figuresize[0], figuresize[1]])
    else:   
        plt.figure()
    colors= [u'#1f77b4', u'#ff7f0e', u'#2ca02c', "darkred", u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22', u'#17becf']

   
    for idx, label in enumerate(df.columns[1:], start=1):
        original_column = df[label] 
        color = colors[original_column_ids[idx] - 1]
        timestamps_in_data = pd.to_datetime(df['time'], unit='s')
        if dateFormat:
            xfmt = mdates.DateFormatter('%d-%m-%y \n %H:%M:%S')
            ax=plt.gca()
            ax.xaxis.set_major_formatter(xfmt)
            plt.plot(timestamps_in_data, original_column, label=label, color=color, markersize=0.7)
        else: 
            plt.plot(timestamps, original_column, label=label, color=color)
            if filtering_type != None:
                extracted_column = GetNthColumn(extracted_values, idx)
                plt.plot(extracted_timestamps, extracted_column, label="extracted "+label, marker="o", linestyle="None", markersize=0.7, color="red")

        plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
        plt.ylim(-90,100)
        if not dateFormat:
            if file=="cods_h_6016" or file =="cods_v_6016":
                plt.xlim(1501192800, 1501273924)
        if file=="cods_h_7299" or file=="cods_v_7299":
            plt.ylim(-10,20)
        plt.title(file)    
        plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
        
        if filtering_type != None:
            plt.show()  
            extracted_column = GetNthColumn(extracted_values, idx)
            if addPrecycle==True:
                time_precycle, current_precycle = ReadPrecycle(MCBY_precycle)
                extracted_column = list(current_precycle.values) + list(extracted_column)
            
            x_axistimestemps=range(len(extracted_column))                
            plt.plot(x_axistimestemps, extracted_column, label="Special cycle "+label, marker="o", linestyle="None", markersize=0.7, color=color)

            plt.title(file)    
            plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
            plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
            plt.xlabel('Time [s]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
            plt.ylim(-90,100)
            plt.xlim(0,15000)
            plt.show()
    if filtering_type == None:
        plt.show()          
    return df

def FillFromFile(filtering_file):
    return filtering_file.split("_")[-1]

def HorizontalFiltering(fill):
    if fill =="6016":
        precycle = [1501206136, 1501206973]
        injection=[1501209270, 1501209276]
        jump1 = [1501210652, 1501210678]
        jump2 = [1501211029, 1501211047]
        ramp = [1501212278, 1501213482]
        spike1 = [1501213764, 1501213845]
        spike2 = [1501213981, 1501214075]
        spike3 = [1501214795, 1501214859]
        scan1 = [1501215485, 1501216762]
        scan2 =  [1501219548,1501220814]
        jump3 = [1501221077, 1501221082]
        spike4 = [1501222949, 1501223037]
        scan3 = [1501223643, 1501225342]
        dropstart = [1501225538, 1501225580]
        dropend = [1501228132,1501228160]
        spike5 = [1501228279, 1501228343]
        spike6 = [1501241435, 1501241525]
        spike7 = [1501241627, 1501241691]
        scan4 = [1501241852, 1501243126]
        spike8 = [1501245062, 1501245127]
        spike9 = [1501251234, 1501251297]
        scan5 = [1501251438, 1501252715]
        spike10 = [1501254546, 1501254682]
        scan6 = [1501254743, 1501255725]
        scan7 = [1501257070, 1501258094]
        intervals = [precycle, injection, jump1, jump2, ramp, spike1, spike2, spike3, scan1, scan2, jump3,
                      spike4  , scan3, dropstart, dropend, spike5, spike6, spike7, scan4, spike8, spike9, scan5, spike10, scan6, scan7]
    elif fill == "7299":
        bump1 = [1539496915, 1539497275]
        bump2 = [1539502563, 1539503011]
        scan1 = [1539503555, 1539504281]
        scan2 = [1539505581, 1539506317]
        spike1 = [1539507499, 1539507609]
        spike2 = [1539507718, 1539507831]
        scan3 = [1539507927, 1539508931]
        spike3 = [1539510643, 1539510730] 
        intervals = [bump1, bump2, scan1, scan2, spike1, spike2, scan3, spike3]
    else:
        print("Unknown fill {}".format(fill))
        sys.exit()
    checkIntervals(intervals, FilteringType.HORIZONTAL)
    return intervals

def VerticalFiltering(fill):
    if fill =="6016":
        precycle = [1501206136, 1501206973]
        injectionV=[1501209269, 1501209273]
        jump1 = [1501210652, 1501210678]
        jump2 = [1501211029, 1501211047]
        
        spike1 = [1501213764, 1501213845]
        spike2V = [1501213887, 1501214078]
        
        rampV = [1501212278, 1501213596]
        spike3V = [1501214722, 1501214787]
        scan1V = [1501217733, 1501219019]
        scan2V = [1501221219, 1501222499]
        spike4V = [1501222876, 1501222941]
    
        dropstartV = [1501223034, 1501223098]
        dropendV =   [1501225587, 1501225623]
        scan3V = [1501225798, 1501227510]
        spike5V = [1501228205, 1501228270]
        spike6V = [1501241362, 1501241428]
        spike7V = [1501241520, 1501241620]
        scan4V = [1501243486, 1501244771]
        spike8V = [1501244993, 1501245062]
        spike9V = [1501251160, 1501251311]
        scan5V = [1501253012, 1501254305]
        spike10V = [1501254473, 1501254679]
        scan6V = [1501255874, 1501256971]
        scan7V = [1501258237, 1501259258]
        intervals = [precycle, injectionV, jump1, jump2, rampV, spike1, spike2V, spike3V, scan1V, scan2V, spike4V, dropstartV, dropendV, scan3V, spike5V, spike6V, spike7V, scan4V, spike8V, spike9V,scan5V, 
                spike10V, scan6V, scan7V]
    elif fill == "7299":
        #
        intervals = []
    else:
        print("Unknown fill {}".format(fill))
        sys.exit()
    checkIntervals(intervals, FilteringType.VERTICAL)
    return intervals

def checkIntervals(intervals, filtering_type):
    for i in range(len(intervals)):
        if intervals[i][0] > intervals[i][1]:
            print("Empty interval [{}-{}]".format(intervals[i][0], intervals[i][1]))
            sys.exit()
        if i + 1 < len(intervals) and intervals[i][1] >= intervals[i+1][0]:
            print("Error on interval: [{}-{}] and [{}-{}]".format(intervals[i][0], intervals[i][1], intervals[i+1][0], intervals[i+1][1]))
            sys.exit()
    total_len = 0
    for interval in intervals:
        total_len += interval[1] - interval[0]
    diff_min=None
    diff=0
    for i in range(len(intervals)-1):
        diff=intervals[i+1][0]-intervals[i][1]
        print(diff)
        if diff_min==None or diff < diff_min:
            diff_min=diff
            
    print("For {}: time={}, segments={}, diff_min={}".format(filtering_type, total_len, len(intervals), diff_min))

def FilteringIntervals(filtering_type, fill):
    if filtering_type == FilteringType.HORIZONTAL:
        return HorizontalFiltering(fill)
    else:
        return VerticalFiltering(fill)

def ExtractedData(df, file, filtering_type):
    buffer = 60
    fill = FillFromFile(file)
    filtering_intervals = FilteringIntervals(filtering_type, fill)
    new_dvalue = []
#     for i, t in enumerate(df['time']):
#         for interval_id, interval in enumerate(filtering_intervals):
#             start_buffer = buffer if interval_id > 0 else 30
#             if t >= interval[0] - start_buffer and t <= interval[1] + buffer:
#                 new_dvalue.append(df.values[i])
    for i, t in enumerate(df['time']):
        exists = False
        for interval_id, interval in enumerate(filtering_intervals):
            start_buffer = buffer if interval_id > 0 else 30
            if t >= interval[0] - start_buffer and t <= interval[1] + buffer:
                exists = True
                break
        if exists:
            new_dvalue.append(df.values[i])
    return new_dvalue

def ReadCSVFile(file, columns=None):
    full_path = "{}{}.csv".format(directory, file)
    df = pd.read_csv(full_path)

    columns_to_write = list(df.columns)
    extracted_values = df.values
    interesting_extracted_values = extracted_values
    interesting_columns = range(len(columns_to_write))
    if columns:
        interesting_columns = [0]
        helper_columns = ['time']
        for column in columns:
            idx = columns_to_write.index(column)
            interesting_columns.append(idx)
            helper_columns.append(df.columns[idx])
        columns_to_write = helper_columns
        print(columns_to_write)
        interesting_extracted_values = []
        for extracted_value in extracted_values:
            interesting_extracted_value = extracted_value[interesting_columns]
            interesting_extracted_values.append(interesting_extracted_value)
    return pd.DataFrame(interesting_extracted_values, columns = columns_to_write), interesting_columns
        
def SaveExtractedData(file, filtering_type, columns=None, addPrecycle = False):
    df, original_column_ids = ReadCSVFile(file, columns)
    
    extracted_values = ExtractedData(df, file, filtering_type)
        # prepend add precycle to extracted values
        
    if addPrecycle:
        time_precycle, current_precycle = ReadPrecycle(MCBY_precycle)
        precycle_values = []
        for current in current_precycle:
            precycle_value = [current] * len(df.columns)
            precycle_values.append(precycle_value)
        extracted_values = precycle_values + list(extracted_values)
        
    for i in range(0,len(extracted_values)):
        extracted_values[i][0] = i

    extracted_df = pd.DataFrame(extracted_values, columns = df.columns)
    
    extracted_df.to_csv("{}Extracted\\{}{}{}.csv".format(directory, filtering_type, columns if columns!=None else "", file))    
# columns = ["MCBCH.5R1.B1"]
# files = [
# #       "cods_h_6016",
# #      "cods_v_6016",
# #     "cods_h_6380",
#  "cods_h_7299",
# # "cods_v_7299",
# #  "cods_h_7441", "cods_v_6380",   "cods_v_7441"
# ]
 
# for file in files:
#     df = ReadFile(file,  columns= ["MCBYH.4L1.B2"], dateFormat=False,filtering_type=FilteringType.HORIZONTAL,  addPrecycle=True)
#     df = ReadFile(file, columns = columns)
#
#     df = ReadFile(file, dateFormat=True, filtering_type=FilteringType.HORIZONTAL)

ReadFile("cods_h_6016", dateFormat=True)
# ReadFile("cods_h_7299", columns= ["MCBYH.4L1.B2"],filtering_type=FilteringType.HORIZONTAL)

# Fill 6016, MCBC 
# Special test cycle 1
# df = ReadFile("cods_h_6016", columns=["MCBCH.5R1.B1"], filtering_type=FilteringType.HORIZONTAL)
# df = ReadFile("cods_v_6016", columns=["MCBCV.5R1.B2"], filtering_type=FilteringType.HORIZONTAL)
# # Special test cycle 2
# df = ReadFile("cods_h_6016", columns=["MCBCH.6R1.B2"], filtering_type=FilteringType.HORIZONTAL)
# df = ReadFile("cods_v_6016", columns=["MCBCV.6R1.B1"], filtering_type=FilteringType.HORIZONTAL)
# # Special test cycle 3
# df = ReadFile("cods_v_6016", columns=["MCBCV.5R1.B2"], filtering_type=FilteringType.VERTICAL)
# df = ReadFile("cods_h_6016", columns=["MCBCH.5R1.B1"], filtering_type=FilteringType.VERTICAL)
# # Special test cycle 4
# df = ReadFile("cods_v_6016", columns=["MCBCV.6L1.B2"], filtering_type=FilteringType.VERTICAL)
# df = ReadFile("cods_h_6016", columns=["MCBCH.6L1.B1"], filtering_type=FilteringType.VERTICAL)



#     SaveExtractedData(file,FilteringType.HORIZONTAL)
#     df = ReadFile(file,  showCompared=False, dateFormat=True)

# Save MCBC created special cycles
# SaveExtractedData("cods_h_6016", FilteringType.HORIZONTAL, ["MCBCH.5R1.B1"])
# SaveExtractedData("cods_v_6016", FilteringType.HORIZONTAL, ["MCBCV.5R1.B2"])
# 
# SaveExtractedData("cods_h_6016", FilteringType.HORIZONTAL, ["MCBCH.6R1.B2"])
# SaveExtractedData("cods_v_6016", FilteringType.HORIZONTAL, ["MCBCV.6R1.B1"])
# 
# SaveExtractedData("cods_v_6016", FilteringType.VERTICAL, ["MCBCV.5R1.B2"])
# SaveExtractedData("cods_h_6016", FilteringType.VERTICAL, ["MCBCH.5R1.B1"])
# 
# SaveExtractedData("cods_v_6016", FilteringType.VERTICAL, ["MCBCV.6L1.B2"])
# SaveExtractedData("cods_h_6016", FilteringType.VERTICAL, ["MCBCH.6L1.B1"])






# Fill 6016, MCBY 
# Special test cycle 1:
df = ReadFile("cods_h_6016", columns=["MCBYH.4L1.B2"], filtering_type=FilteringType.HORIZONTAL)
df = ReadFile("cods_v_6016", columns=["MCBYV.4L1.B1"], filtering_type=FilteringType.HORIZONTAL)
# MCBY Special test cycle 2:
df = ReadFile("cods_h_6016", columns=["MCBYH.B4R1.B2"], filtering_type=FilteringType.HORIZONTAL)
df = ReadFile("cods_v_6016", columns=["MCBYV.B4R1.B1"], filtering_type=FilteringType.HORIZONTAL)

# Save MCBY created special cycles
# SaveExtractedData("cods_h_6016", FilteringType.HORIZONTAL, ["MCBYH.4L1.B2"])
# SaveExtractedData("cods_v_6016", FilteringType.HORIZONTAL, ["MCBYV.4L1.B1"])
# 
# SaveExtractedData("cods_h_6016", FilteringType.HORIZONTAL, ["MCBYH.B4R1.B2"])
# SaveExtractedData("cods_v_6016", FilteringType.HORIZONTAL, ["MCBYV.B4R1.B1"])

# ReadPrecycle(MCBY_precycle)
# Fill 7299, MCBY
# Special test cycle 1:
# df = ReadFile("cods_h_7299", columns=["MCBYH.4L1.B2"], filtering_type=FilteringType.HORIZONTAL, addPrecycle=True)
# df = ReadFile("cods_v_7299", columns=["MCBYV.4L1.B1"], filtering_type=FilteringType.HORIZONTAL, addPrecycle=True)
# Save MCBY created special cycles
# SaveExtractedData("cods_h_7299", FilteringType.HORIZONTAL, ["MCBYH.4L1.B2"], addPrecycle=True)
# SaveExtractedData("cods_v_7299", FilteringType.HORIZONTAL, ["MCBYV.4L1.B1"], addPrecycle=True)

# ReadFile("cods_h_6016", columns=["MCBYH.4L1.B2"], filtering_type=FilteringType.HORIZONTAL)
# SaveExtractedData("cods_h_6016", FilteringType.HORIZONTAL, ["MCBYH.4L1.B2"])

import cx_Oracle
import os
import pandas as pd

class Database:
    def __init__(self):
        os.environ['TNS_ADMIN'] = '/eos/project/o/oracle/public/admin/'
        user = 'magmeas'
        pw = 'Welovemagnets2020'
        db = 'cerndb1'
        dsn = cx_Oracle.makedsn('pdb-s.cern.ch',10121,service_name='PDB_CERNDB1.cern.ch',
                                region=None, sharding_key=None, super_sharding_key=None)
        self.conn = cx_Oracle.connect(user,pw,dsn)
    
    def runQuery(self,query):
        df_ora = pd.read_sql(query,con=self.conn)
        return df_ora
    
    def getRunsByWONumber(self,woNumber):
        if type(woNumber) is not list: woNumber = [ woNumber ]
        wovars = ','.join(':%d' % i for i in range(len(woNumber)))
        query = """
SELECT ar.mpd_edms_id, 
       ar.mpd_run_wo,
       acr.mpd_run_type, 
       mpd_reference_radius, 
       acp.mpd_aperture_nr, 
       mpd_keywords, 
       mpd_segment_name, 
       mpd_fdi_abs, 
       mpd_fdi_cmp 
FROM   analysis_run ar 
       LEFT JOIN analysis_part ap 
              ON ap.mpd_analysis_run_id = ar.id 
       JOIN acquisition_run acr 
         ON acr.mpd_run_wo = ar.mpd_run_wo 
       JOIN acquisition_part acp 
         ON ap.mpd_acquisition_part_id = acp.id 
WHERE  ar.mpd_run_wo IN({0})""".format(wovars)
        df_ora = pd.read_sql(query,con=self.conn,params=woNumber)
        return df_ora
    
    def getRunsByEdmsIds(self,edmsIds):
        if type(edmsIds) is not list: edmsIds = [ edmsIds ]
        edms_vars = ','.join(':%d' % i for i in range(len(edmsIds)))
        query = """
SELECT ar.mpd_edms_id, 
       ar.mpd_run_wo,
       acr.mpd_run_type, 
       mpd_reference_radius, 
       acp.mpd_aperture_nr, 
       mpd_keywords, 
       mpd_segment_name, 
       mpd_fdi_abs, 
       mpd_fdi_cmp 
FROM   analysis_run ar 
       LEFT JOIN analysis_part ap 
              ON ap.mpd_analysis_run_id = ar.id 
       JOIN acquisition_run acr 
         ON acr.mpd_run_wo = ar.mpd_run_wo 
       JOIN acquisition_part acp 
         ON ap.mpd_acquisition_part_id = acp.id 
WHERE  ar.mpd_edms_id IN({0})""".format(edms_vars)
        df_ora = pd.read_sql(query,con=self.conn,params=edmsIds)
        return df_ora
    
    def getRunData(self,edmsIds):
        """Load The Rundata entries (turn values) for all given EDMS ids"""
        if type(edmsIds) is not list: edmsIds = [ edmsIds ]
        in_vars = ','.join(':%d' % i for i in range(len(edmsIds)))
        query = """
SELECT   t.workorder_id, 
         t.edms_id, 
         t.aperture_nr, 
         t.x, 
         t.y, 
         t.z, 
         t.rotation_speed, 
         t.reference_radius, 
         t.keywords, 
         t.segment_name, 
         t.fdi_abs, 
         t.fdi_cmp, 
         t.time, t.duration,
         t.average_current_1, t.average_current_2, t.average_current_3, t.average_current_4, 
         t.ramp_rate_1, t.ramp_rate_2, t.ramp_rate_3, t.ramp_rate_4, 
         t.main_field_b, t.main_field_a, 
         t.angle, t.dx, t.dy, 
         t.b1,t.b2, t.b3,t.b4, t.b5, t.b6, t.b7,t.b8, t.b9, t.b10, t.b11, t.b12, t.b13,t.b14, t.b15, 
         t.a1, t.a2,t.a3, t.a4, t.a5,t.a6,t.a7,t.a8,  t.a9, t.a10, t.a11,t.a12,t.a13, t.a14, t.a15 
FROM     analysis_results_view_abs t 
WHERE    EDMS_ID IN({0})
ORDER BY time ASC        
        """.format(in_vars)
        
        df_ora = pd.read_sql(query,con=self.conn,params=edmsIds)
        df_ora.set_index(['EDMS_ID',"APERTURE_NR","SEGMENT_NAME"],inplace=True)
        df_ora['Turn'] = df_ora.groupby(['EDMS_ID','FDI_ABS']).cumcount()+1
        return df_ora
    
    def getSegmentRunData(self,edmsIds,ap,segments):
        if type(edmsIds) is not list: edmsIds = [ edmsIds ]
        if type(segments) is not list: segments = [ segments ]
        edms_vars = ','.join(':%d' % i for i in range(len(edmsIds)))
        ap_var = ':{0}'.format(len(edmsIds))
        segment_vars = ','.join(':{0}'.format(i+1+len(edmsIds)) for i in range(len(segments)))
        query = """
SELECT   t.workorder_id, 
         t.edms_id, 
         t.aperture_nr, 
         t.x, 
         t.y, 
         t.z, 
         t.rotation_speed, 
         t.reference_radius, 
         t.keywords, 
         t.segment_name, 
         t.fdi_abs, 
         t.fdi_cmp, 
         t.time, t.duration,
         t.average_current_1, t.average_current_2, t.average_current_3, t.average_current_4, 
         t.ramp_rate_1, t.ramp_rate_2, t.ramp_rate_3, t.ramp_rate_4, 
         t.main_field_b, t.main_field_a, 
         t.angle, t.dx, t.dy, 
         t.b1,t.b2, t.b3,t.b4, t.b5, t.b6, t.b7,t.b8, t.b9, t.b10, t.b11, t.b12, t.b13,t.b14, t.b15, 
         t.a1, t.a2,t.a3, t.a4, t.a5,t.a6,t.a7,t.a8,  t.a9, t.a10, t.a11,t.a12,t.a13, t.a14, t.a15 
FROM     analysis_results_view_abs t 
WHERE    EDMS_ID IN({0})
AND      APERTURE_NR={1}
AND      SEGMENT_NAME IN({2})
ORDER BY time ASC        
        """.format(edms_vars,ap_var,segment_vars)
        param_list = edmsIds
        param_list.append(ap)
        param_list.extend(segments)
        df_ora = pd.read_sql(query,con=con,params=param_list)
        df_ora.set_index(['EDMS_ID',"APERTURE_NR","SEGMENT_NAME"],inplace=True)
        # Add Turn number
        df_ora['Turn'] = df_ora.groupby(['EDMS_ID','FDI_ABS']).cumcount()+1
        return df_ora
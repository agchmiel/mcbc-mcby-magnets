import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import math
import enum 
from enum import Enum
from AnalyzeMeasurements import Directory, SeparateBranches, ComputeField, Residuals
from FitHysteresis import FitEzio, a_original, b_original, c_original
from cycler import cycler
import PlottingParameters as plparam
from scipy.optimize.minpack import curve_fit
import AnalyzeMeasurements



colors = ["b","g", "r", "c", "m", "y", "k", "silver"]
plt.rcParams['axes.prop_cycle'] = plt.cycler(color=colors) 

Ic = 15000
T = 4.5
Tmeas = 4.5
Tc0= 9.5

class Magnet(Enum):
    MCBC90 = "MCBC90"
    MCBY39 = "MCBY39"

magnet_to_params = {
    Magnet.MCBC90: {
        "best_fit": "AChmielinska_tf",
        "component_2": [0.0001, 0.44,  1.6715,  2],
        "component_3": [-0.4203 * 1e-3,3.5519 * 3, 70, 0.1657* 1e-3, 1.7023 * 10, 60],
        "component_5": [4.5*10**(-4), 1.1*10**(-3), 3],
        "current_filtering": 31,
        "reference": Directory.REFERENCE,
    },
    Magnet.MCBY39: {
        "best_fit": "2021_tf_mcby",
        "component_2": [0.0001, 0.44,  1.6715,  2],
        "component_3": [-0.4203 * 1e-3,3.5519 * 3, 70, 0.1657* 1e-3, 1.7023 * 10, 60],
        "component_5": [4.5*10**(-4), 1.1*10**(-3), 3],
        "current_filtering": 26,
        "reference": Directory.STC2_MCBY,
    },
}


fits = {
    "AChmielinska_tf": {
#         #  Original from AnalyzeMeasurements,  current_filtering=30
#         #  bounds=(0, [1, 1, 2., 3]), INJECTION_CURRENT=5:
#         #  Filtered up and down
#         "y": 0.028610608751229817,  
#         "u": 1.02287051e-04 ,
#         "p": 9.51016077e-01,
#         "q": 1.99999874e+00,
#         "h": 2.00000000e+00,
#         "sigma1":-4.57036447e-04, 
#         "S1":   4.35726793e+00,
#         "I01":7.89791019e+01,
#         "sigma2": -3.64894467e-05,
#         "S2":   2.99805882e+00 ,
#         "I02": 4.95711778e+01,
#         "rho":0,
#         "r": 0,
#         "INJECTION_CURRENT": 5,
#         "NOMINAL_CURRENT": 80,
#         "delta": -1,
#         "a": 4.25555578e-04,
#         "b": 2.31258696e-03,
#         "c": 6.27532404e+00,      
        
#     Eventual set more symmetric: first column: rounded, second: original, parameters as on 29.01.2021 FidEl meeting with corrected sign on 01.02
    "y": 0.02861, #2.8607e-02,  
     "u": 0.00010,  #1.0233e-04,
    "p": 0.95,  #9.5108e-01,
    "q": 1.99,#1.9999e+00,
    "h": 2.00, #2.0000e+00,
    "sigma1": 0.00049,#4.9680e-04, 
    "S1": 4.09, # 4.0908e+00,
    "I01": 79.23,#7.9229e+01,
    "sigma2": 0.00002,#1.5352e-05,
    "S2": 6.28, #6.2794e+00 ,
    "I02": 38.77, #3.9577e+01,
    "rho": 0,
    "r":0,
    "INJECTION_CURRENT": 5,
    "NOMINAL_CURRENT": 80,
    "delta": -1, 
    "a": 0.00043,#4.2679e-04,
    "b": 0.00229, #2.2880e-03,
    "c": 6.23#6.2296e+00,

    },
    "AChmielinska_tf_mcby_reference": { # evaluated for REFERENCE_MCBY, but not used due to data acquisition problems around zero 
        "y": 0.03258, #2.8607e-02,  
        "u": 0.00011,
        "p": 0.88,
        "q": 0.0067,
        "h": 2.00, #2.0000e+00,
        "sigma1": 0.00125,#4.9680e-04, 
        "S1": 3.68, # 4.0908e+00,
        "I01": 70.71,#7.9229e+01,
        "sigma2": 0.00042,#1.5352e-05,
        "S2": 6.59, #6.2794e+00 ,
        "I02": 46.43, #3.9577e+01,
        "rho": 0,
        "r":0,
        "INJECTION_CURRENT": 5,
        "NOMINAL_CURRENT": 77,
        "delta": -1, 
        "a": 0.00042,#4.2679e-04,
        "b": 0.00238, #2.2880e-03,
        "c": 9.24#6.2296e+00,

    },
    "2021_tf_mcby": {
#     Eventual set more symmetric: first column: rounded, second: original, parameters as on 29.01.2021 FidEl meeting with corrected sign on 01.02
        "y": 0.03258, #2.8607e-02,  
        "u": 0.00009,
        "p": 0.96,
        "q": 1.99,
        "h": 2.00, #2.0000e+00,
        "sigma1": 0.00129,#4.9680e-04, 
        "S1": 3.61, # 4.0908e+00,
        "I01": 69.47,#7.9229e+01,
        "sigma2": 0.00036,#1.5352e-05,
        "S2": 6.92, #6.2794e+00 ,
        "I02": 45.71, #3.9577e+01,
        "rho": 0,
        "r":0,
        "INJECTION_CURRENT": 5,
        "NOMINAL_CURRENT": 77,
        "delta": -1, 
        "a": 0.00042,#4.2679e-04,
        "b": 0.00174, #2.2880e-03,
        "c": 3.93#6.2296e+00,

    },
       
       
    "sammut_tf": {
        "y": 10.1141,
        "u": -0.0055,
        "p": 0.4487,
        "q": 1.6715,
        "h": 2,
        "sigma1": -0.4203,
        "S1": 3.5519,
        "I01": 13239,
        "sigma2": 0.1657,
        "S2": 1.7023,
        "I02": 9735,
        "rho": 0.0037,
        "r": 1.3992,
        "INJECTION_CURRENT": 760,
        "NOMINAL_CURRENT": 11850,
        "delta": 0, 
        "a": 0,
        "b": 0,
        "c": 0,
    },
    "sammut_b2": {
        "y":  1.3601,
        "u":  0.154,
        "p": 1.532,
        "q":  0.929,
        "h":  2,
        "sigma1":  -3.241,
        "S1":  8.088,
        "I01":  8568,
        "sigma2":  20.131,
        "S2":  25.551,
        "I02":  14107,
        "rho":  -0.182,
        "r":  1.953,
        "INJECTION_CURRENT": 760,
        "NOMINAL_CURRENT": 11850,
        "delta": 0, 
        "a": 0,
        "b": 0,
        "c": 0,
    },
    "prab_tf": {
        "y": 10.119,
        "u": -0.005,
        "p": 1.11,
        "q": -0.29,
        "h": 2,
        "sigma1": 0.247,
        "S1": 1.691,
        "I01": 10739,
        "sigma2": -0.545,
        "S2": 3.23,
        "I02": 13599,
        "rho": 0.003,
        "r": 1.86,
        "INJECTION_CURRENT": 760,
        "NOMINAL_CURRENT": 11850,
        "delta": 0, 
        "a": 0,
        "b": 0,
        "c": 0,
    },
    "prab_b2": {
        "y": 0.142,
        "u": 0.154,
        "p": 1.54,
        "q": 0.96,
        "h": 2,
        "sigma1": -3.241,
        "S1": 8.088,
        "I01": 8569,
        "sigma2": 20.131,
        "S2": 25.551,
        "I02": 14107,
        "rho": -0.182,
        "r": 1.95,
        "INJECTION_CURRENT": 760,
        "NOMINAL_CURRENT": 11850,
        "delta": 0, 
        "a": 0,
        "b": 0,
        "c": 0,
    },
     "2009_tf_1.9K": {
        "y": 2.863e-2 ,
        "u": 0,
        "p": 0,
        "q": 0,
        "h": 0,
        "sigma1": 0.00151,
        "S1": 4.483,
        "I01": 92.76,
        "sigma2": 0,
        "S2": 0,
        "I02": 0,
        "rho": 0,
        "r": 0,
        "INJECTION_CURRENT": 0,
        "NOMINAL_CURRENT": 100,
        "delta": 0, 
        "a": 0,
        "b": 0,
        "c": 0,
    },
    
    "AChmielinska_tf_not_rounded": {
#     Parameters from 01/02/2021 to compare with rounded numbers
    "y":  0.028610608751229817,
     "u": 1.02295469e-04,
    "p": 9.51019802e-01 ,
    "q": 1.99776458e+00,
    "h": 2.00000000e+00,
    "sigma1": 4.77784107e-04,
    "S1": 4.23650847e+00,
    "I01": 7.89737163e+01,
    "sigma2": 2.33391917e-05,
    "S2":  4.01276130e+00 ,
    "I02": 4.19753644e+01,
    "rho": 0,
    "r":0,
    "INJECTION_CURRENT": 5,
    "NOMINAL_CURRENT": 80,
    "delta": -1, 
    "a": 4.25764385e-04,
    "b": 2.30842389e-03,
    "c": 6.26756932e+00,
    },
    
    "2009_tf_1.9K_mcby": {
        "y": 3.26e-2 ,
        "u": 0,
        "p": 0,
        "q": 0,
        "h": 0,
        "sigma1": 0.00213,
        "S1": 3.194,
        "I01": 70.66,
        "sigma2": 0,
        "S2": 0,
        "I02": 0,
        "rho": 0,
        "r": 0,
        "INJECTION_CURRENT": 0,
        "NOMINAL_CURRENT": 100,
        "delta": 0, 
        "a": 0,
        "b": 0,
        "c": 0,
    },
}


def negativeU(params):
    new_params = params.copy()
    new_params['u'] = -params['u']
    new_params['delta'] = -params['delta']
    return new_params

def BmGeometricToOptimize(I, ym,b):
    return ym*I+b

def BmGeometric(I, ym):
    return ym*I
    
def TGeometric(I, ym):
    return BmGeometric(I, ym) / I
 
def cnGeometric(I, yn):
    vec=[]
    for i in I:
        vec.append(yn)
    return vec

def BmDCMagnetizationFixedCurrent(I, um, p, q, h):
    return BmDCMagnetization(I, um, p, q, h, 5)

def BmDCMagnetization(I, um, p, q, h, INJECTION_CURRENT):
    res = []
    for i in I:
        res.append(BmDCMagnetizationSingle(i, um, p, q, h, INJECTION_CURRENT))
    return res
     
def BmDCMagnetizationSingle(I, um, p, q, h, INJECTION_CURRENT):
#     return 0
    factor1 = (INJECTION_CURRENT/(abs(I)))
    factor2 = ((Ic - abs(I))/(Ic - INJECTION_CURRENT))
    factor3 = ((Tc0**1.7-T**1.7) / (Tc0**1.7 - Tmeas**1.7))
#     print(I, factor1 ** (2 - p), factor2 ** (q), factor3 ** h)
#     print("Current: {}, factor1: {}, factor2: {}, mu: {}".format(I, factor1,factor2, um))
    return um * abs(I) * factor1**(2-p) * factor2**q * factor3**h

def BmDCMagnetizationEzioMinus(I, a, b, c):
    return BmDCMagnetizationEzio(I, -1, a, b, c)

def BmDCMagnetizationEzioPlus(I, a, b, c):
    return BmDCMagnetizationEzio(I, 1, a, b, c)

def BmDCMagnetizationEzio(I, delta, a, b, c):
    upper_fit = [FitEzio(i, delta, a, b, c) for i in I]
    return upper_fit

def TDCMagnetizationEzio(I, delta, a, b, c):
    return BmDCMagnetizationEzio(I, delta, a, b, c)/I
    
def TDCMagnetization(I, u, p, q, h, INJECTION_CURRENT): 
    return BmDCMagnetization(I, u, p, q, h, INJECTION_CURRENT)/I

def cnDCMagnetization(I, un, pn, qn, hn, INJECTION_CURRENT): 
    factor1 = (INJECTION_CURRENT/I)
    factor2 = ((Ic - I)/(Ic - INJECTION_CURRENT))
    factor3 = ((Tc0**1.7-T**1.7) / (Tc0**1.7 - Tmeas**1.7))
    return un *  factor1**(2-pn) * factor2**(qn) * factor3**(hn)


def ComputeArcus(I, Si, I0i, NOMINAL_CURRENT):
    return 1/np.pi * np.arctan(Si * (I-I0i)/NOMINAL_CURRENT) + 0.5

def ComputeErrF(I, Si, I0i, NOMINAL_CURRENT):
    result=[]
    for i in I:
        arg=(abs(i)-I0i)/NOMINAL_CURRENT
        result.append(-0.5*(1+math.erf(Si*arg)))
    return result

def BmSaturation(I, sigma1, S1, I01, sigma2, S2, I02, func, NOMINAL_CURRENT):
    if func == "Arc":
        factor1 = ComputeArcus(I, S1, I01, NOMINAL_CURRENT)
        factor2 = ComputeArcus(I, S2, I02, NOMINAL_CURRENT)
        
    if func == "ErrF":
        factor1 = ComputeErrF(I, S1, I01, NOMINAL_CURRENT)
        factor2 = ComputeErrF(I, S2, I02, NOMINAL_CURRENT)
    return sigma1*I*factor1+ sigma2*I*factor2

def BmSaturationErrF(I, sigma1, S1, I01, sigma2, S2, I02):
    factor1 = ComputeErrF(I, S1, I01, 80)
    factor2 = ComputeErrF(I, S2, I02, 80)
    return sigma1*I*factor1+ sigma2*I*factor2

def BmResidual(I, rho, rm, INJECTION_CURRENT):
    return I*rho*(INJECTION_CURRENT/I)*rm

def BmResidualFixedCurrent(I, rho, rm):
    return BmResidual(I, rho, rm, 5)
    
def TSaturation(I, sigma1, S1, I01, sigma2, S2, I02, func, NOMINAL_CURRENT):
    res = BmSaturation(I, sigma1, S1, I01, sigma2, S2, I02, func, NOMINAL_CURRENT)/I
    return res

def cnSaturation(I, sigma1n, S1n, I01n, sigma2n, S2n, I02n, func, NOMINAL_CURRENT):
    return BmSaturation(I, sigma1n, S1n, I01n, sigma2n, S2n, I02n, func, NOMINAL_CURRENT)/I


def TResidual(I, rho, rm, INJECTION_CURRENT):
    return BmResidual(I, rho, rm, INJECTION_CURRENT)/I

def cnResidual(I, rhon, rn, INJECTION_CURRENT):
    return rhon*(INJECTION_CURRENT/I)**rn

def BmIntegral(I, params, func, components):
    print(params)
    return TIntegral(I, params, func, components) * I

def TIntegral(I, params, func, components):
    static_components = {
        1:   TGeometric(I, params['y']),
        2:   TDCMagnetization(I,  params['u'],  params['p'],  params['q'],  params['h'], params['INJECTION_CURRENT']),
        3:   TSaturation(I,  params['sigma1'],  params['S1'],  params['I01'],  params['sigma2'],  params['S2'],  params['I02'],  func, params['NOMINAL_CURRENT']),
        4:   TResidual(I,  params['rho'],  params['r'], params['INJECTION_CURRENT']),
        5:   TDCMagnetizationEzio(I, params['delta'], params['a'], params['b'], params['c'])
    }
    lists=[]
    for i in components :
        lists.append(static_components[i])
    value = [sum(x) for x in zip(*lists)]
    return value  

def cnIntegral(I, params, func, components):
    static_components = {
        1:   cnGeometric(I, params['y']),
        2:   cnDCMagnetization(I,  params['u'],  params['p'],  params['q'],  params['h'], params['INJECTION_CURRENT']),
        3:   cnSaturation(I,  params['sigma1'],  params['S1'],  params['I01'],  params['sigma2'],  params['S2'],  params['I02'],  func, params['NOMINAL_CURRENT']),
        4:   cnResidual(I,  params['rho'],  params['r'], params['INJECTION_CURRENT']),
    }
    lists=[]
    for i in components :
        lists.append(static_components[i])
    value = [sum(x) for x in zip(*lists)]
    return value   
    

def PlotcnIntegral(I, func, components, key):
    plt.plot(I, cnIntegral(I, fits[key], func, components), label= "{}: {} (Ramp down)".format(key, components))
    plt.plot(I, cnIntegral(I, negativeU(fits[key]), func, components), label= "{}: {} (Ramp up)".format(key, components))
    plt.ylabel("b2 (units)", fontsize=plparam.LABEL_FONTSIZE)
    plt.xlabel("Current (A)", fontsize=plparam.LABEL_FONTSIZE)
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)   
    plt.show()
 
 
def FilterDataAboveZero(I, ydata):
    I_above_zero=[]
    y_data_above_zero=[]
    for idx, value in enumerate(I):
        if (value) > 0:
            I_above_zero.append(value)
            y_data_above_zero.append(ydata[idx])
#     print("ABOVE ZERO")

    return I_above_zero, y_data_above_zero

def FilterDataBelowZero(I, ydata):
    I_below_zero=[]
    y_data_below_zero=[]
    for idx, value in enumerate(I):
        if (value) < 0:
            I_below_zero.append(value)
            y_data_below_zero.append(ydata[idx])
    return   I_below_zero, y_data_below_zero
        
def PlotTIntegral(I, func, components, key, idx, should_show=True, ylim=None):   

    I_above_zero, y_above_zero = FilterDataAboveZero(I, TIntegral(I, fits[key], func, components))
    I_below_zero, y_below_zero = FilterDataBelowZero(I, TIntegral(I, fits[key], func, components))
    plt.plot(I_below_zero,  y_below_zero, label= "{}: {} (Ramp down)".format(key, components), color=colors[2*idx])
    plt.plot(I_above_zero,  y_above_zero, label= None, color=colors[2*idx])

    I_above_zero2, y_above_zero2 = FilterDataAboveZero(I, TIntegral(I, negativeU(fits[key]), func, components))
    I_below_zero2, y_below_zero2 = FilterDataBelowZero(I, TIntegral(I, negativeU(fits[key]), func, components))
    plt.plot(I_above_zero2,  y_above_zero2, label= None, color=colors[2*idx + 1])
    plt.plot(I_below_zero2,  y_below_zero2, label= "{}: {} (Ramp up)".format(key, components), color=colors[2*idx + 1])

#      
#     
#     plt.plot(I, TIntegral(I, fits[key], func, components), label= "{}: {} (Ramp down)".format(key, components))
#     plt.plot(I, TIntegral(I, negativeU(fits[key]), func, components), label= "{}: {} (Ramp up)".format(key, components), color="blue")
    plt.ylabel("TF (Tm/A)", fontsize=plparam.LABEL_FONTSIZE)
    plt.xlabel("Current (A)", fontsize=plparam.LABEL_FONTSIZE)
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
#
    if ylim:
        plt.ylim(ylim[0], ylim[1])
    if should_show:
        plt.show()

def PlotBmIntegral(I, func, components, key, should_show=True):    
    plt.plot(I, BmIntegral(I, fits[key], func, components), label= "{}: {} (Ramp down)".format(key, components))
    plt.plot(I, BmIntegral(I, negativeU(fits[key]), func, components), label= "{}: {} (Ramp up)".format(key, components))
    plt.ylabel("B (Tm)", fontsize=plparam.LABEL_FONTSIZE)
    plt.xlabel("Current (A)", fontsize=plparam.LABEL_FONTSIZE)
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)                 
    if should_show:
        plt.show()

def PlotImprovedFitPython(current, ydata, component, p0_array, ramp):   
    if ramp == "down":
        delta = -1
    else:
        delta = 1
    sorted_current = np.sort(current)
    
    if component  == "Geometric":
        popt, pcov = curve_fit(BmGeometricToOptimize, current, ydata, p0_array)
        plt.plot(sorted_current, BmGeometricToOptimize(sorted_current, *popt), marker='None', markersize=3, linestyle="-",label=component + "_fit"+ " (Ramp "+ramp+")")
    
    if component == "Saturation":     
        popt, pcov = curve_fit(BmSaturationErrF, current, ydata, p0_array)
        plt.plot(sorted_current, BmSaturationErrF(sorted_current, *popt), marker='None', markersize=3, linestyle="-",label=component + "_fit"+ " (Ramp "+ramp+")")
    elif component == "DC_Magnetization_Ezio":
        if delta == -1:
            funct = BmDCMagnetizationEzioMinus
        else:
            funct = BmDCMagnetizationEzioPlus
        popt, pcov = curve_fit(funct, current, ydata, p0_array)
        plt.plot(sorted_current, funct(sorted_current, *popt), marker='None', markersize=3, linestyle="--",label=component + "_fit"+ " (Ramp "+ramp+")")
        
    elif component  == "DC_Magnetization_FiDeL":
        popt, pcov = curve_fit(BmDCMagnetizationFixedCurrent, current, ydata, p0_array, bounds=(0, [1, 1, 2., 3]))
        perr = np.sqrt(np.diag(pcov))
        print("perr: {}".format(perr))
        plt.plot(sorted_current, BmDCMagnetizationFixedCurrent(sorted_current, *popt), marker='None', markersize=3, linestyle="-",label=component + "_fit"+ " (Ramp "+ramp+")")
    
    elif component == "Residual":
        popt, pcov = curve_fit(BmResidualFixedCurrent, current, ydata, p0_array)
        plt.plot(sorted_current, BmResidualFixedCurrent(sorted_current, *popt), marker='None', markersize=3, linestyle="-",label=component + "_fit"+ " (Ramp "+ramp+")")
    print("fit", component, ramp, popt)
    return popt
                                                                             

def FilterYdata(current, ydata, filter_value_max):
    ydata_filtered=[]
    current_filtered=[]
    for idx, value in enumerate(current):
        if abs(value) < filter_value_max:
            current_filtered.append(value)
            ydata_filtered.append(ydata[idx])
    return current_filtered, ydata_filtered    

def MeasurementsWithFit(magnet, specify_DC_magnetization):
    magnet_in_analyze_measurements = AnalyzeMeasurements.MAGNET
    assert(magnet.value == magnet_in_analyze_measurements)
    
    ramp_down_file_data, ramp_up_file_data = SeparateBranches(magnet_to_params[magnet]["reference"])
    time_down, current_down, integrated_down = ComputeField(ramp_down_file_data)
    time_up, current_up, integrated_up = ComputeField(ramp_up_file_data)
    
    plt.figure(1)
    plt.plot(current_down, integrated_down/current_down,  label="Data (Ramp down)", marker="x", color="blue", linestyle="None")
    plt.plot(current_up, integrated_up/current_up, label="Data (Ramp up)", marker="x", color="red", linestyle="None")
    plt.show()
    
    components = [1]
    linear_fit_down = BmIntegral(current_down, fits[magnet_to_params[magnet]["best_fit"]], "ErrF", components)
    linear_fit_up = BmIntegral(current_up, fits[magnet_to_params[magnet]["best_fit"]], "ErrF", components)

    plt.plot(current_down, integrated_down - linear_fit_down, marker='o', markersize=3, linestyle="None", color="blue", label="Data (Ramp down) - Linear_fit")
    plt.plot(current_up, integrated_up - linear_fit_up, marker='o', markersize=3, linestyle="None", color="red", label="Data (Ramp up)- Linear_fit")

    plt.ylabel('Integrated B1 - Integrated B1 (fit) [Tm]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
   
    ydata_down = integrated_down - linear_fit_down
    ydata_up = integrated_up - linear_fit_up
      
      
      
    current_filtered_down, ydata_filtered_down = FilterYdata(current_down, ydata_down, magnet_to_params[magnet]["current_filtering"])
    current_filtered_up, ydata_filtered_up = FilterYdata(current_up, ydata_up, magnet_to_params[magnet]["current_filtering"])
    for idx in range(len(current_filtered_up)):
        current_filtered_down.append(current_filtered_up[idx])
        ydata_filtered_down.append(-ydata_filtered_up[idx])
    
    if specify_DC_magnetization == 2:
        mag_pts = PlotImprovedFitPython(current_filtered_down, ydata_filtered_down, "DC_Magnetization_FiDeL",  magnet_to_params[magnet]["component_2"], "down")
        ydata_minus_linear_and_magnetization_down = ydata_down - BmDCMagnetizationFixedCurrent(current_down, *mag_pts)
    elif specify_DC_magnetization == 5:    
        mag_pts = PlotImprovedFitPython(current_filtered_down, ydata_filtered_down, "DC_Magnetization_Ezio",  magnet_to_params[magnet]["component_5"], "down")
        ydata_minus_linear_and_magnetization_down = ydata_down - BmDCMagnetizationEzioMinus(current_down, *mag_pts)

    plt.plot(current_down, ydata_minus_linear_and_magnetization_down, label="Data (Ramp down) - Linear_fit - Magnetization_fit", linestyle="None", marker='x', markersize=3)
    sat_pts = PlotImprovedFitPython(current_down, ydata_minus_linear_and_magnetization_down, "Saturation",  magnet_to_params[magnet]["component_3"], "down")

    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.title(magnet_to_params[magnet]["best_fit"])
    plt.show()

# def CalculateRelativeDelta

def PlotError(magnet, components, key_array, in_percent=False):
    ramp_down_file_data, ramp_up_file_data = SeparateBranches(magnet_to_params[magnet]["reference"])
    time_down, current_down, integrated_down = ComputeField(ramp_down_file_data)
    time_up, current_up, integrated_up = ComputeField(ramp_up_file_data)
    Nominal_field = AnalyzeMeasurements.GetNominalField(magnet_to_params[magnet]["reference"])

    plt.figure(figsize=[8,4])
    for key in key_array:
        Aga_model_down = BmIntegral(current_down, fits[key], "ErrF", components)
        Aga_model_up = BmIntegral(current_up, negativeU(fits[key]), "ErrF", components)
#     old_model_down = BmIntegral(current_down, fits["2009_tf_1.9K"], "ErrF", components)
#     old_model_up = BmIntegral(current_up, fits["2009_tf_1.9K"], "ErrF", components)
#     
        if in_percent:
            delta_rel_Aga_down = (integrated_down-Aga_model_down)/Nominal_field*10**2
            delta_rel_Aga_up = (integrated_up-Aga_model_up)/Nominal_field*10**2
        else:
            delta_rel_Aga_down = (integrated_down-Aga_model_down)/Nominal_field*10**4
            delta_rel_Aga_up = (integrated_up-Aga_model_up)/Nominal_field*10**4
#     delta_rel_old_down =  (integrated_down-old_model_down)/2.27*10**4
#     delta_rel_old_up =(integrated_up-old_model_up)/2.27*10**4

        plt.plot(current_down, delta_rel_Aga_down, marker = "x", linestyle = "None", label = "{} {} (Ramp down)".format(key, components))
        plt.plot(current_up, delta_rel_Aga_up, marker = "x", linestyle = "None", label = "{} {} (Ramp up)".format(key, components))
#     plt.plot(current_down, delta_rel_old_down, marker = "x", linestyle = "None", label = "2009 {} (Ramp down)".format(components))
#     plt.plot(current_up, delta_rel_old_up, marker = "x", linestyle = "None", label = "2009 {} (Ramp up)".format(components))

    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.0, fontsize=plparam.LEGEND_FONTSIZE)  
    label = u'$\Delta$$_{r}$ [%]' if in_percent else u'$\Delta$$_{r}$ [units]'
    plt.ylabel( label,  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    if in_percent:
        plt.ylim(-0.2,0.25)
    else:
        plt.ylim(-20,25) 
    plt.show()
    
    plt.figure(figsize=[8,4])
    for key in key_array:
        Aga_model_down = BmIntegral(current_down, fits[key], "ErrF", components)
        Aga_model_up = BmIntegral(current_up, negativeU(fits[key]), "ErrF", components)
        delta_rel_Aga_down = (integrated_down-Aga_model_down)/Nominal_field*10**4
        delta_rel_Aga_up = (integrated_up-Aga_model_up)/Nominal_field*10**4
        plt.plot(current_down, abs(delta_rel_Aga_down), marker = "x", linestyle = "None", label = "{} {} (Ramp down)".format(key, components))
        plt.plot(current_up, abs(delta_rel_Aga_up), marker = "x", linestyle = "None", label = "{} {} (Ramp up)".format(key, components))
#     plt.plot(current_down, abs(delta_rel_old_down), marker = "x", linestyle = "None", label = "2009 {} (Ramp down)".format(components))
#     plt.plot(current_up, abs(delta_rel_old_up), marker = "x", linestyle = "None", label = "2009 {} (Ramp up)".format(components))


    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE) 
    plt.ylabel(u'|$\Delta$$_{r}|$ [units]' ,  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.ylim(-1,25)
    plt.show()
    
    
def PlotMeasurementsResidualAndFit(magnet, components_array, key,  zoom=None, zoomAsym=None):
    ramp_down_file_data, ramp_up_file_data = SeparateBranches(magnet_to_params[magnet]["reference"])
    time_down, current_down, integrated_down = ComputeField(ramp_down_file_data)
    time_up, current_up, integrated_up = ComputeField(ramp_up_file_data)
    
    components = [1]
    linear_fit_down = BmIntegral(current_down, fits[key], "ErrF", components)
    linear_fit_up = BmIntegral(current_up, fits[key], "ErrF", components)
    
    ydata_down = integrated_down - linear_fit_down
    ydata_up = integrated_up - linear_fit_up
    plt.figure(1)
    plt.title("Residuals")
    if zoom:
        plt.xlim(-zoom[0], zoom[0])
        plt.ylim(-zoom[1], zoom[1])
        plt.title("Residuals: Zoom")
    if zoomAsym:
        plt.xlim(zoomAsym[0], zoomAsym[1])
        plt.ylim(zoomAsym[2], zoomAsym[3])
        plt.title("Residuals: Zoom")    
    plt.plot(current_down, ydata_down, label="Ramp down - [1]", linestyle="None", marker='x', markersize=3)
    plt.plot(current_up, ydata_up, label="Ramp up - [1]", linestyle="None", marker='x', markersize=3)
    I=np.linspace(-80, 80, 1000)
    PlotBmIntegral(I, "ErrF", components_array, key)
    plt.show()
 
def PlotFitComparison(components_array, key_array, zoomAsym=None):     
    I=np.linspace(-80,80, 100)
    for idx, key in enumerate(key_array):
        PlotTIntegral(I, "ErrF", components_array, key, idx, should_show=False)
    if zoomAsym:
        plt.xlim(zoomAsym[0], zoomAsym[1])
        plt.ylim(zoomAsym[2], zoomAsym[3])
    plt.show()
    for key in key_array:
        PlotBmIntegral(I, "ErrF", components_array, key, should_show=False)
    if zoomAsym:
        plt.xlim(zoomAsym[0], zoomAsym[1])
        plt.ylim(zoomAsym[2], zoomAsym[3])
    plt.show()
# I=np.linspace(680, 12000, 1000)      
# PlotTIntegral(I, func="Arc", components=[1,2,3,4], key="sammut_tf") 
 
# MeasurementsWithFit(5)
PlotMeasurementsResidualAndFit(Magnet.MCBC90, components_array=[2,3], key="AChmielinska_tf")
# # PlotMeasurementsResidualAndFit(components_array=[2,3], key="AChmielinska_tf", zoom=[30, 0.002])
# # PlotMeasurementsResidualAndFit(components_array=[2,3], key="AChmielinska_tf", zoomAsym=[-82, -30, -0.00186, 0.023])
# # 
# MeasurementsWithFit(5)
# PlotMeasurementsResidualAndFit(components_array=[3,5], key="AChmielinska_tf")
# PlotMeasurementsResidualAndFit(components_array=[3,5], key="AChmielinska_tf", zoom=[30, 0.002])
# PlotMeasurementsResidualAndFit(components_array=[3,5], key="AChmielinska_tf",  zoomAsym=[-82, -30, -0.00186, 0.023])
# 
# # PlotFitComparison([1], ["AChmielinska_tf", "2009_tf_1.9K"], [-100, 100, 0.02860, 0.02864])
# # PlotFitComparison([2,3,4], ["AChmielinska_tf", "2009_tf_1.9K"])
# 
# # PlotError([1,2,3,4], ["2009_tf_1.9K"])
# # PlotMeasurementsResidualAndFit(components_array=[3], key="2009_tf_1.9K")
# # PlotError([1,3,4,5], [ "AChmielinska_tf", "2009_tf_1.9K"])
# # PlotMeasurementsResidualAndFit(components_array=[2,3,4], key="AChmielinska_tf")
# PlotError([1,3,4,5], [ "AChmielinska_tf", "2009_tf_1.9K"])
# MeasurementsWithFit(Magnet.MCBY39, 5)
# PlotMeasurementsResidualAndFit(components_array=[2,3], key="AChmielinska_tf_mcby")
# MeasurementsWithFit(Magnet.MCBY39, 2)
# PlotMeasurementsResidualAndFit(Magnet.MCBY39, components_array=[2,3], key="AChmielinska_tf_mcby")
# PlotError(Magnet.MCBY39, [1,3,5], [ "2021_tf_mcby", "2009_tf_1.9K_mcby"],  in_percent=True)
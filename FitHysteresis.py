import numpy as np
import matplotlib.pyplot as plt

a_original = 4.5*10**(-4)
b_original =  1.1*10**(-3)
c_original = 3

def FitEzio(current,delta, a = a_original, b = b_original, c= c_original):
    L = 0.9
#     print((a + b/c) *L)
    
    if current>=0 and delta<=0:
        return (a+b/(current+c))*L
    if current<=0 and delta<=0:
        return (a-b/(current-c))*L
    if current>0 and delta>0:
        return -(a+b/(current+c))*L
    if current<0 and delta>0:
        return -(a-b/(current-c))*L

def CalculateEzioFit(a = a_original, b = b_original, c= c_original):
    current_array = np.arange(-80, 80, 0.01)
    upper_fit = [FitEzio(current, -1, a, b, c) for current in current_array]
    lower_fit = [FitEzio(current, 1, a, b, c) for current in current_array]
    x = np.concatenate((current_array, current_array))
    y = upper_fit + lower_fit
    return x, y

def ShowEzioFit():
    x, y = CalculateEzioFit()
    plt.plot(x, y, linestyle="None", marker="x")
    plt.show()
# ShowEzioFit()
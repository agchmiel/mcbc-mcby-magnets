import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import PlottingParameters as plparam
from scipy import interpolate
from numpy import linspace

# The input for this program is the created special cycle based on operational data using CreateSpecialCycle. This input includes ~14500 points for fill 6016
# Later, this program uses interpolation to minimise number of points and special filters to reduced it further.
# Finally, it divides the data into two sets after 7900 seconds, so thus each set contains < 2000 points



directory = "C:\\Users\\agchmiel\\cernbox\\Documents\\Papers\\Magnets\\MCBC\\Data\\Extracted\\"

def ReadCSVFile(file):
    full_path = "{}{}.csv".format(directory, file)
    df = pd.read_csv(full_path)
    return df

def SaveFileIntoParitions(file):
    df = ReadCSVFile(file)
    timestamps = df['time']
    label=df.columns[2]
    data = df[label]

    filtered_timestamps, filtered_data = FinalFiltering(timestamps, data)
    filtered_data = RoundNumber(filtered_data)
    filtered_timestamps = RoundNumber(filtered_timestamps)
    
    
    partition_timestamp =  7900#2 * 60 * 60 # 2 hours
    zipped = zip(filtered_timestamps, filtered_data)
    part1 = []
    part2 = []
    for item in zipped:
        if item[0] < partition_timestamp:
            part1.append(item)
        else:
            part2.append(item)
    last_time = part1[-1][0]
    part2.insert(0, part1[-1])
    part2_improved = []
    for item_in_part2 in part2:
        part2_improved.append((item_in_part2[0] - last_time, item_in_part2[1]))
    
    WriteZippedData(part1, "_part1")
    WriteZippedData(part2_improved, "_part2")

def WriteZippedData(values, suffix=""):
    extracted_df = pd.DataFrame(values, columns = ['Time', 'Current'])
    extracted_df.to_csv("{}{}_Reduced{}.csv".format(directory, file, suffix), index=False)  
    
def SaveFilteredData(file):
    df = ReadCSVFile(file)
    timestamps = df['time']
    label=df.columns[2]
    data = df[label]

    filtered_timestamps, filtered_data = FinalFiltering(timestamps, data)
    filtered_data = RoundNumber(filtered_data)
    filtered_timestamps = RoundNumber(filtered_timestamps)
    values = zip(filtered_timestamps, filtered_data)
    
    WriteZippedData(values)

def DerivativeAtIndex(index, timestamps, data):
    if timestamps[index] <= timestamps[index - 1]:
        print(data[index] , data[index-1], timestamps[index], timestamps[index-1])
    return (data[index] - data[index-1]) / (timestamps[index] - timestamps[index-1])

def Derivative(timestamps, data):
    derivative = []
    derivative_timestamps = []
    for i in range(1, len(timestamps)):
        derivative_element = DerivativeAtIndex(i, timestamps, data)
        derivative.append(derivative_element)
        derivative_timestamps.append(timestamps[i])
    return derivative_timestamps, derivative

def Variance(data):
    variance_array = data
    r = 2
    for i in range(r, len(data) - r):
        slicer = data[i-r:i+r]
#         print(slicer)
        variance_array[i] = np.var(slicer)
    return variance_array

def IsSimilarDerivative(i, timestamps, data):
    if i + 1 == len(timestamps):
        return False;
    my_derivative = DerivativeAtIndex(i, timestamps, data)
    next_derivative = DerivativeAtIndex(i + 1, timestamps, data)
    return abs(my_derivative - next_derivative) < 0.05

def FilterDataWithInterp1(timestamps, data):
    f = interpolate.interp1d(timestamps, data)
    filtered_timestamps = np.linspace(timestamps[0], timestamps[len(timestamps) - 1], num=3100)
    values = f(filtered_timestamps)
    return filtered_timestamps, values

def addPoint(timestamp, datum, timestamps, data, aaaa):
#     print("Adding {}: {} -> {}".format(aaaa, timestamp, datum))
    timestamps.append(timestamp)
    data.append(datum)

def FilterData(timestamps, data):
    print(len(data))
    filtered_data =[data[0]]
    filtered_timestamps =[timestamps[0]]
    want_to_take_next_point = False
    for x in range(1, len(data)):
#         if timestamps[x] > 50:
#             break
        difference=abs(data[x]-filtered_data[-1])
#         if difference > 1.:
#             print("huge: ({},{}) -> ({},{})".format(filtered_timestamps[-1], filtered_data[-1],timestamps[x], data[x]))
        if difference > 0.08: # and not IsSimilarDerivative(x, timestamps, data):
            if filtered_timestamps[-1] < timestamps[x - 1]:
                addPoint(timestamps[x-1], data[x-1], filtered_timestamps, filtered_data, "a")
            addPoint(timestamps[x], data[x], filtered_timestamps, filtered_data, "b")
            want_to_take_next_point = True
        elif want_to_take_next_point:
            addPoint(timestamps[x], data[x], filtered_timestamps, filtered_data, "c")
            want_to_take_next_point = False
        elif timestamps[x] - filtered_timestamps[-1] > 3:
            addPoint(timestamps[x], data[x], filtered_timestamps, filtered_data, "d")
    return filtered_timestamps, filtered_data

def FinalFiltering(timestamps, data):
    print(len(timestamps))
    filtered_timestamps, filtered_data = FilterDataWithInterp1(timestamps, data)
    return FilterData(filtered_timestamps, filtered_data)

def RoundNumber(filtered_array):
    rounded=[]
    for i in filtered_array:
        a=round(i,2)
        rounded.append(a)
    return rounded

def ReadData(file):   
    df = ReadCSVFile(file)
    timestamps = df['time']
    label=df.columns[2]
    data = df[label]
    plt.plot(timestamps, data, label="Special cycle "+label, marker="o", linestyle="None", markersize=0.7, color=LabelToColor[label])
    plt.title(file)    
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.ylabel('Current [A]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.xlabel('Time [s]',  {'color': 'black', 'fontsize': plparam.LABEL_FONTSIZE})
    plt.ylim(-90,100)
    
    filtered_timestamps, filtered_data = FinalFiltering(timestamps, data)
    filtered_timestamps = RoundNumber(filtered_timestamps)
    filtered_data = RoundNumber(filtered_data)
    
#     plt.plot(filtered_timestamps, filtered_data, label="Filtered"+label, linestyle="-", markersize=1.5, color="red")
    plt.plot(filtered_timestamps, filtered_data, label="Filtered"+label,  linestyle="-", markersize=1.5, color="green")
    
#     plt.plot(filtered_timestamps2, filtered_data2, label="Filtered "+label, linestyle="-", markersize=1.5, color="black")
    print("Selected points red: "+str(len(filtered_data)))
#     print("Selected points black: " + str(len(filtered_data2)))
    plt.show()
#     return
    
    filtered_derivate_timestamps, filtered_derivative_array = Derivative(filtered_timestamps, filtered_data)
    plt.plot(filtered_derivate_timestamps, filtered_derivative_array, label="Special cycle "+label, marker="o", linestyle="None", markersize=0.7, color=LabelToColor[label])
    plt.title("Filtered first derivative")
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.show()

    derivative_timestamps, derivative_array = Derivative(timestamps, data)
    plt.plot(derivative_timestamps, derivative_array, label="Special cycle "+label, marker="o", linestyle="None", markersize=0.7, color=LabelToColor[label])
    plt.xlim(0,15000)
    print("label")
    plt.title("Original first derivative")
    plt.legend(shadow=True, loc='upper center', ncol=2, handlelength=1.5, fontsize=plparam.LEGEND_FONTSIZE)
    plt.show()
        
        
        
files = [
# "FilteringType.HORIZONTAL['MCBCH.5R1.B1']cods_h_6016", "FilteringType.HORIZONTAL['MCBCV.5R1.B2']cods_v_6016",
# "FilteringType.HORIZONTAL['MCBCH.6R1.B2']cods_h_6016", "FilteringType.HORIZONTAL['MCBCV.6R1.B1']cods_v_6016",
# "FilteringType.VERTICAL['MCBCV.5R1.B2']cods_v_6016", "FilteringType.VERTICAL['MCBCH.5R1.B1']cods_h_6016",
# "FilteringType.VERTICAL['MCBCV.6L1.B2']cods_v_6016", "FilteringType.VERTICAL['MCBCH.6L1.B1']cods_h_6016",
# "FilteringType.HORIZONTAL['MCBYH.4L1.B2']cods_h_6016", "FilteringType.HORIZONTAL['MCBYV.4L1.B1']cods_v_6016",
# "FilteringType.HORIZONTAL['MCBYH.B4R1.B2']cods_h_6016", "FilteringType.HORIZONTAL['MCBYV.B4R1.B1']cods_v_6016",
"FilteringType.HORIZONTAL['MCBYH.4L1.B2']cods_h_7299", "FilteringType.HORIZONTAL['MCBYV.4L1.B1']cods_v_7299", 
]

# colors= [u'#1f77b4', u'#ff7f0e', u'#2ca02c', u'#d62728', u'#9467bd', u'#8c564b', u'#e377c2', u'#7f7f7f', u'#bcbd22', u'#17becf']

LabelToColor = {
    "MCBCH.5R1.B1": u'#1f77b4',
    "MCBCV.5R1.B2": u'#8c564b',
    "MCBCH.6R1.B2": u'#e377c2',
    "MCBCV.6R1.B1": u'#ff7f0e',
    "MCBCV.6L1.B2": u'#e377c2',
    "MCBCH.6L1.B1": u'#ff7f0e',
    "MCBYH.4L1.B2": u'#bcbd22',
    "MCBYV.4L1.B1": "darkred",
    "MCBYH.B4R1.B2": u'#7f7f7f',
    'MCBYV.B4R1.B1':  u'#2ca02c',
}
for file in files:
    ReadData(file)
#     SaveFilteredData(file)
#     SaveFileIntoParitions(file)
